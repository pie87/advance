//	HYPE.documents["global-offices-mapa"]

(function HYPE_DocumentLoader() {
	var resourcesFolderName = "global-offices-mapa.hyperesources";
	var documentName = "global-offices-mapa";
	var documentLoaderFilename = "globalofficesmapa_hype_generated_script.js";
	var mainContainerID = "globalofficesmapa_hype_container";

	// find the URL for this script's absolute path and set as the resourceFolderName
	try {
		var scripts = document.getElementsByTagName('script');
		for(var i = 0; i < scripts.length; i++) {
			var scriptSrc = scripts[i].src;
			if(scriptSrc != null && scriptSrc.indexOf(documentLoaderFilename) != -1) {
				resourcesFolderName = scriptSrc.substr(0, scriptSrc.lastIndexOf("/"));
				break;
			}
		}
	} catch(err) {	}

	// Legacy support
	if (typeof window.HYPE_DocumentsToLoad == "undefined") {
		window.HYPE_DocumentsToLoad = new Array();
	}
 
	// load HYPE.js if it hasn't been loaded yet
	if(typeof HYPE_160 == "undefined") {
		if(typeof window.HYPE_160_DocumentsToLoad == "undefined") {
			window.HYPE_160_DocumentsToLoad = new Array();
			window.HYPE_160_DocumentsToLoad.push(HYPE_DocumentLoader);

			var headElement = document.getElementsByTagName('head')[0];
			var scriptElement = document.createElement('script');
			scriptElement.type= 'text/javascript';
			scriptElement.src = resourcesFolderName + '/' + 'HYPE.js?hype_version=160';
			headElement.appendChild(scriptElement);
		} else {
			window.HYPE_160_DocumentsToLoad.push(HYPE_DocumentLoader);
		}
		return;
	}
	
	// handle attempting to load multiple times
	if(HYPE.documents[documentName] != null) {
		var index = 1;
		var originalDocumentName = documentName;
		do {
			documentName = "" + originalDocumentName + "-" + (index++);
		} while(HYPE.documents[documentName] != null);
		
		var allDivs = document.getElementsByTagName("div");
		var foundEligibleContainer = false;
		for(var i = 0; i < allDivs.length; i++) {
			if(allDivs[i].id == mainContainerID && allDivs[i].getAttribute("HYPE_documentName") == null) {
				var index = 1;
				var originalMainContainerID = mainContainerID;
				do {
					mainContainerID = "" + originalMainContainerID + "-" + (index++);
				} while(document.getElementById(mainContainerID) != null);
				
				allDivs[i].id = mainContainerID;
				foundEligibleContainer = true;
				break;
			}
		}
		
		if(foundEligibleContainer == false) {
			return;
		}
	}
	
	var hypeDoc = new HYPE_160();
	
	var attributeTransformerMapping = {b:"i",c:"i",bC:"i",d:"i",aS:"i",M:"i",e:"f",aT:"i",N:"i",f:"d",O:"i",g:"c",aU:"i",P:"i",Q:"i",aV:"i",R:"c",bG:"f",aW:"f",aI:"i",S:"i",bH:"d",l:"d",aX:"i",T:"i",m:"c",bI:"f",aJ:"i",n:"c",aK:"i",bJ:"f",X:"i",aL:"i",A:"c",aZ:"i",Y:"bM",B:"c",bK:"f",bL:"f",C:"c",D:"c",t:"i",E:"i",G:"c",bA:"c",a:"i",bB:"i"};
	
	var resources = {"3":{n:"mapa.jpg",p:1},"4":{n:"santiago.png",p:1},"0":{n:"barcelona.png",p:1},"5":{n:"saopaulo.png",p:1},"1":{n:"boston.png",p:1},"6":{n:"global-offices-mapa.jpg",p:1},"2":{n:"budapest.png",p:1}};
	
	var scenes = [{x:0,p:"600px",c:"#FFFFFF",v:{"55":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#DE2A00",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"30",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:331,aL:11,b:372},"10":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"10",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:563,aL:11,b:316},"48":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"23",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:263,aL:11,b:167},"33":{o:"content-box",h:"6",x:"visible",a:0,q:"100% 100%",b:0,j:"absolute",r:"none",c:990,k:"div",z:"9",d:520},"40":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"16",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:521,aL:11,b:205},"56":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#DE2A00",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"31",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:267,aL:11,b:394},"11":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"11",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:610,aL:11,b:246},"49":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"24",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:235,aL:11,b:197},"41":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"17",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:512,aL:11,b:164},"42":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"18",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:491,aL:11,b:168},"2":{o:"content-box",h:"0",x:"visible",a:316,q:"100% 100%",b:204,j:"absolute",r:"inline",c:144,k:"div",z:"4",d:38,e:"0.000000"},"43":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"19",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:499,aL:11,b:138},"36":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"12",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:679,aL:11,b:237},"3":{o:"content-box",h:"1",x:"visible",a:0,q:"100% 100%",b:164,j:"absolute",r:"inline",c:271,k:"div",z:"5",d:32,e:"0.000000"},"50":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"25",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:200,aL:11,b:205},"51":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"26",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:178,aL:11,b:253},"4":{o:"content-box",h:"2",x:"visible",a:512,q:"100% 100%",b:30,j:"absolute",r:"inline",c:112,k:"div",z:"6",d:151,e:"0.000000"},"37":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"13",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:750,aL:11,b:300},"44":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"20",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:466,aL:11,b:177},"52":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#DE2A00",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"27",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:510,aL:11,b:178},"45":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"21",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:456,aL:11,b:165},"6":{o:"content-box",h:"3",x:"visible",a:0,q:"100% 100%",b:0,j:"absolute",r:"inline",c:990,k:"div",z:"2",d:520,bL:"11.875000",e:"0.000000"},"38":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"14",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:791,aL:11,b:219},"53":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#DE2A00",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"28",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:461,aL:11,b:197},"46":{c:3,d:3,I:"None",r:"inline",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",k:"div",O:0,B:"#A0A0A0",P:0,z:"22",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:436,aL:11,b:204},"39":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#012A52",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"15",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:834,aL:11,b:192},"8":{o:"content-box",h:"4",x:"visible",a:130,q:"100% 100%",b:365,j:"absolute",r:"inline",c:138,k:"div",z:"7",d:33,e:"0.000000"},"54":{c:3,d:3,I:"None",e:"0.000000",J:"None",K:"None",g:"#DE2A00",L:"None",M:0,N:0,aI:11,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",k:"div",O:0,P:0,z:"29",C:"#A0A0A0",D:"#A0A0A0",aK:11,aJ:11,a:271,aL:11,b:188},"9":{o:"content-box",h:"5",x:"visible",a:335,q:"100% 100%",b:377,j:"absolute",r:"inline",c:126,k:"div",z:"8",d:68,e:"0.000000"}},n:"Untitled Scene",T:{kTimelineDefaultIdentifier:{d:4.2,i:"kTimelineDefaultIdentifier",n:"Main Timeline",a:[{f:"2",t:0,d:1,i:"bL",e:"0.000000",s:"11.875000",o:"6"},{f:"2",t:0,d:1,i:"e",e:"1.000000",s:"0.000000",o:"6"},{f:"2",t:1,d:0.08,i:"d",e:8,s:3,o:"10"},{f:"2",t:1,d:0.08,i:"a",e:560,s:563,o:"10"},{f:"2",t:1,d:0.08,i:"b",e:313,s:316,o:"10"},{f:"2",t:1,d:0.08,i:"c",e:8,s:3,o:"10"},{f:"2",t:1,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"10"},{f:"2",t:1.03,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"11"},{f:"2",t:1.03,d:0.08,i:"d",e:8,s:3,o:"11"},{f:"2",t:1.03,d:0.08,i:"c",e:8,s:3,o:"11"},{f:"2",t:1.07,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"36"},{f:"2",t:1.07,d:0.08,i:"d",e:8,s:3,o:"36"},{f:"2",t:1.07,d:0.08,i:"c",e:8,s:3,o:"36"},{f:"2",t:1.1,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"37"},{f:"2",t:1.1,d:0.08,i:"d",e:8,s:3,o:"37"},{f:"2",t:1.1,d:0.08,i:"c",e:8,s:3,o:"37"},{f:"2",t:1.13,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"38"},{f:"2",t:1.13,d:0.08,i:"d",e:8,s:3,o:"38"},{f:"2",t:1.13,d:0.08,i:"c",e:8,s:3,o:"38"},{f:"2",t:1.16,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"39"},{f:"2",t:1.16,d:0.08,i:"d",e:8,s:3,o:"39"},{f:"2",t:1.16,d:0.08,i:"c",e:8,s:3,o:"39"},{f:"2",t:1.19,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"40"},{f:"2",t:1.19,d:0.08,i:"d",e:8,s:3,o:"40"},{f:"2",t:1.19,d:0.08,i:"c",e:8,s:3,o:"40"},{f:"2",t:1.21,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"41"},{f:"2",t:1.21,d:0.08,i:"d",e:8,s:3,o:"41"},{f:"2",t:1.21,d:0.08,i:"c",e:8,s:3,o:"41"},{f:"2",t:1.24,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"42"},{f:"2",t:1.24,d:0.08,i:"d",e:8,s:3,o:"42"},{f:"2",t:1.24,d:0.08,i:"c",e:8,s:3,o:"42"},{f:"2",t:1.27,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"43"},{f:"2",t:1.27,d:0.08,i:"d",e:8,s:3,o:"43"},{f:"2",t:1.27,d:0.08,i:"c",e:8,s:3,o:"43"},{f:"2",t:1.29,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"44"},{f:"2",t:1.29,d:0.08,i:"d",e:8,s:3,o:"44"},{f:"2",t:1.29,d:0.08,i:"c",e:8,s:3,o:"44"},{f:"2",t:2.02,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"45"},{f:"2",t:2.02,d:0.08,i:"d",e:8,s:3,o:"45"},{f:"2",t:2.02,d:0.08,i:"c",e:8,s:3,o:"45"},{f:"2",t:2.05,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"46"},{f:"2",t:2.05,d:0.08,i:"d",e:8,s:3,o:"46"},{f:"2",t:2.05,d:0.08,i:"c",e:8,s:3,o:"46"},{f:"2",t:2.08,d:0.08,i:"c",e:8,s:3,o:"48"},{f:"2",t:2.08,d:0.08,i:"d",e:8,s:3,o:"48"},{f:"2",t:2.08,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"48"},{f:"2",t:2.11,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"49"},{f:"2",t:2.11,d:0.08,i:"d",e:8,s:3,o:"49"},{f:"2",t:2.11,d:0.08,i:"c",e:8,s:3,o:"49"},{f:"2",t:2.13,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"50"},{f:"2",t:2.13,d:0.08,i:"d",e:8,s:3,o:"50"},{f:"2",t:2.13,d:0.08,i:"c",e:8,s:3,o:"50"},{f:"2",t:2.15,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"51"},{f:"2",t:2.15,d:0.08,i:"d",e:8,s:3,o:"51"},{f:"2",t:2.15,d:0.08,i:"c",e:8,s:3,o:"51"},{f:"2",t:3.12,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"52"},{f:"2",t:3.12,d:0.08,i:"d",e:8,s:3,o:"52"},{f:"2",t:3.12,d:0.08,i:"c",e:8,s:3,o:"52"},{f:"2",t:3.14,d:0.08,i:"c",e:8,s:3,o:"53"},{f:"2",t:3.14,d:0.08,i:"d",e:8,s:3,o:"53"},{f:"2",t:3.14,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"53"},{f:"2",t:3.17,d:0.08,i:"c",e:8,s:3,o:"54"},{f:"2",t:3.17,d:0.08,i:"d",e:8,s:3,o:"54"},{f:"2",t:3.17,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"54"},{f:"2",t:3.18,d:0.02,i:"e",e:"1.000000",s:"0.000000",o:"2"},{f:"2",t:3.2,d:0.08,i:"c",e:8,s:3,o:"55"},{f:"2",t:3.2,d:0.08,i:"d",e:8,s:3,o:"55"},{f:"2",t:3.2,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"55"},{f:"2",t:3.2,d:0.02,i:"e",e:"0.000000",s:"1.000000",o:"2"},{f:"2",t:3.22,d:0.08,i:"c",e:8,s:3,o:"56"},{f:"2",t:3.22,d:0.08,i:"d",e:8,s:3,o:"56"},{f:"2",t:3.22,d:0.08,i:"e",e:"1.000000",s:"0.000000",o:"56"},{f:"2",t:3.22,d:0.02,i:"e",e:"1.000000",s:"0.000000",o:"3"},{f:"2",t:3.22,d:0.02,i:"e",e:"0.000000",s:"0.000000",o:"2"},{f:"2",t:3.24,d:0.02,i:"e",e:"0.000000",s:"1.000000",o:"3"},{f:"2",t:3.24,d:0.02,i:"e",e:"1.000000",s:"0.000000",o:"2"},{f:"2",t:3.26,d:0.02,i:"e",e:"0.000000",s:"0.000000",o:"3"},{f:"2",t:3.26,d:0.02,i:"e",e:"0.000000",s:"1.000000",o:"2"},{f:"2",t:3.27,d:0.02,i:"e",e:"1.000000",s:"0.000000",o:"4"},{f:"2",t:3.28,d:0.02,i:"e",e:"1.000000",s:"0.000000",o:"3"},{f:"2",t:3.28,d:0.02,i:"e",e:"1.000000",s:"0.000000",o:"2"},{f:"2",t:3.29,d:0.02,i:"e",e:"0.000000",s:"1.000000",o:"4"},{f:"2",t:4,d:0.02,i:"e",e:"0.000000",s:"1.000000",o:"3"},{f:"2",t:4.01,d:0.02,i:"e",e:"0.000000",s:"0.000000",o:"4"},{f:"2",t:4.02,d:0.02,i:"e",e:"1.000000",s:"0.000000",o:"8"},{f:"2",t:4.02,d:0.02,i:"e",e:"1.000000",s:"0.000000",o:"3"},{f:"2",t:4.03,d:0.02,i:"e",e:"1.000000",s:"0.000000",o:"4"},{f:"2",t:4.04,d:0.02,i:"e",e:"0.000000",s:"1.000000",o:"8"},{f:"2",t:4.05,d:0.02,i:"e",e:"0.000000",s:"1.000000",o:"4"},{f:"2",t:4.06,d:0.02,i:"e",e:"0.000000",s:"0.000000",o:"8"},{f:"2",t:4.07,d:0.02,i:"e",e:"1.000000",s:"0.000000",o:"4"},{f:"2",t:4.08,d:0.02,i:"e",e:"1.000000",s:"0.000000",o:"9"},{f:"2",t:4.08,d:0.02,i:"e",e:"1.000000",s:"0.000000",o:"8"},{f:"2",t:4.1,d:0.02,i:"e",e:"0.000000",s:"1.000000",o:"9"},{f:"2",t:4.1,d:0.02,i:"e",e:"0.000000",s:"1.000000",o:"8"},{f:"2",t:4.12,d:0.02,i:"e",e:"0.000000",s:"0.000000",o:"9"},{f:"2",t:4.12,d:0.02,i:"e",e:"1.000000",s:"0.000000",o:"8"},{f:"2",t:4.14,d:0.02,i:"e",e:"1.000000",s:"0.000000",o:"9"},{f:"2",t:4.16,d:0.02,i:"e",e:"0.000000",s:"1.000000",o:"9"},{f:"2",t:4.18,d:0.02,i:"e",e:"1.000000",s:"0.000000",o:"9"}],f:30}},o:"1"}];
	
	var javascripts = [];
	
	var functions = {};
	var javascriptMapping = {};
	for(var i = 0; i < javascripts.length; i++) {
		try {
			javascriptMapping[javascripts[i].identifier] = javascripts[i].name;
			eval("functions." + javascripts[i].name + " = " + javascripts[i].source);
		} catch (e) {
			hypeDoc.log(e);
			functions[javascripts[i].name] = (function () {});
		}
	}
	
	hypeDoc.setAttributeTransformerMapping(attributeTransformerMapping);
	hypeDoc.setResources(resources);
	hypeDoc.setScenes(scenes);
	hypeDoc.setJavascriptMapping(javascriptMapping);
	hypeDoc.functions = functions;
	hypeDoc.setCurrentSceneIndex(0);
	hypeDoc.setMainContentContainerID(mainContainerID);
	hypeDoc.setResourcesFolderName(resourcesFolderName);
	hypeDoc.setShowHypeBuiltWatermark(0);
	hypeDoc.setShowLoadingPage(false);
	hypeDoc.setDrawSceneBackgrounds(true);
	hypeDoc.setGraphicsAcceleration(true);
	hypeDoc.setDocumentName(documentName);

	HYPE.documents[documentName] = hypeDoc.API;
	document.getElementById(mainContainerID).setAttribute("HYPE_documentName", documentName);

	hypeDoc.documentLoad(this.body);
}());

