//	HYPE.documents["animacio-box1"]

(function HYPE_DocumentLoader() {
	var resourcesFolderName = "animacio-box1.hyperesources";
	var documentName = "animacio-box1";
	var documentLoaderFilename = "animaciobox1_hype_generated_script.js";
	var mainContainerID = "animaciobox1_hype_container";

	// find the URL for this script's absolute path and set as the resourceFolderName
	try {
		var scripts = document.getElementsByTagName('script');
		for(var i = 0; i < scripts.length; i++) {
			var scriptSrc = scripts[i].src;
			if(scriptSrc != null && scriptSrc.indexOf(documentLoaderFilename) != -1) {
				resourcesFolderName = scriptSrc.substr(0, scriptSrc.lastIndexOf("/"));
				break;
			}
		}
	} catch(err) {	}

	// Legacy support
	if (typeof window.HYPE_DocumentsToLoad == "undefined") {
		window.HYPE_DocumentsToLoad = new Array();
	}
 
	// load HYPE.js if it hasn't been loaded yet
	if(typeof HYPE_160 == "undefined") {
		if(typeof window.HYPE_160_DocumentsToLoad == "undefined") {
			window.HYPE_160_DocumentsToLoad = new Array();
			window.HYPE_160_DocumentsToLoad.push(HYPE_DocumentLoader);

			var headElement = document.getElementsByTagName('head')[0];
			var scriptElement = document.createElement('script');
			scriptElement.type= 'text/javascript';
			scriptElement.src = resourcesFolderName + '/' + 'HYPE.js?hype_version=160';
			headElement.appendChild(scriptElement);
		} else {
			window.HYPE_160_DocumentsToLoad.push(HYPE_DocumentLoader);
		}
		return;
	}
	
	// handle attempting to load multiple times
	if(HYPE.documents[documentName] != null) {
		var index = 1;
		var originalDocumentName = documentName;
		do {
			documentName = "" + originalDocumentName + "-" + (index++);
		} while(HYPE.documents[documentName] != null);
		
		var allDivs = document.getElementsByTagName("div");
		var foundEligibleContainer = false;
		for(var i = 0; i < allDivs.length; i++) {
			if(allDivs[i].id == mainContainerID && allDivs[i].getAttribute("HYPE_documentName") == null) {
				var index = 1;
				var originalMainContainerID = mainContainerID;
				do {
					mainContainerID = "" + originalMainContainerID + "-" + (index++);
				} while(document.getElementById(mainContainerID) != null);
				
				allDivs[i].id = mainContainerID;
				foundEligibleContainer = true;
				break;
			}
		}
		
		if(foundEligibleContainer == false) {
			return;
		}
	}
	
	var hypeDoc = new HYPE_160();
	
	var attributeTransformerMapping = {b:"i",c:"i",bC:"i",d:"i",aS:"i",M:"i",e:"f",aT:"i",N:"i",f:"d",O:"i",g:"c",aU:"i",P:"i",Q:"i",aV:"i",R:"c",bG:"f",aW:"f",aI:"i",S:"i",bH:"d",l:"d",aX:"i",T:"i",m:"c",bI:"f",aJ:"i",n:"c",aK:"i",bJ:"f",X:"i",aL:"i",A:"c",aZ:"i",Y:"bM",B:"c",bK:"f",bL:"f",C:"c",D:"c",t:"i",E:"i",G:"c",bA:"c",a:"i",bB:"i"};
	
	var resources = {"18":{n:"picto15.png",p:1},"10":{n:"picto7.png",p:1},"19":{n:"picto16.png",p:1},"11":{n:"picto8.png",p:1},"0":{n:"fletxa.png",p:1},"12":{n:"picto9.png",p:1},"1":{n:"fons.jpg",p:1},"20":{n:"picto17.png",p:1},"2":{n:"arbre.png",p:1},"13":{n:"picto10.png",p:1},"3":{n:"base.jpg",p:1},"21":{n:"picto18.png",p:1},"14":{n:"picto11.png",p:1},"4":{n:"picto1.png",p:1},"5":{n:"picto2.png",p:1},"15":{n:"picto12.png",p:1},"22":{n:"picto19.png",p:1},"6":{n:"picto3.png",p:1},"23":{n:"picto20.png",p:1},"16":{n:"picto13.png",p:1},"7":{n:"picto4.png",p:1},"8":{n:"picto5.png",p:1},"24":{n:"aparatus.jpg",p:1},"17":{n:"picto14.png",p:1},"9":{n:"picto6.png",p:1}};
	
	var scenes = [{x:0,p:"600px",c:"#FFFFFF",v:{"9":{c:0,d:227,I:"None",r:"inline",J:"None",K:"None",g:"#4A4A4A",L:"None",M:0,N:0,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",P:0,k:"div",O:0,z:"3",C:"#A0A0A0",D:"#A0A0A0",a:237,b:33},"32":{o:"content-box",h:"24",x:"visible",a:142,q:"100% 100%",b:251,j:"absolute",r:"inline",c:211,k:"div",z:"10",d:119,e:"0.000000"},"25":{o:"content-box",h:"17",x:"visible",a:323,q:"100% 100%",b:45,j:"absolute",r:"inline",c:32,k:"div",z:"25",d:26,e:"0.000000"},"18":{o:"content-box",h:"10",x:"visible",a:88,q:"100% 100%",b:130,j:"absolute",r:"inline",c:26,k:"div",z:"18",d:23,e:"0.000000"},"10":{c:0,d:227,I:"None",r:"inline",J:"None",K:"None",g:"#4A4A4A",L:"None",M:0,N:0,A:"#A0A0A0",x:"visible",j:"absolute",B:"#A0A0A0",P:0,k:"div",O:0,z:"4",C:"#A0A0A0",D:"#A0A0A0",a:237,b:40},"26":{o:"content-box",h:"18",x:"visible",a:261,q:"100% 100%",b:93,j:"absolute",r:"none",c:26,k:"div",z:"26",d:24,e:"0.000000"},"19":{o:"content-box",h:"11",x:"visible",a:158,q:"100% 100%",b:142,j:"absolute",r:"inline",c:22,k:"div",z:"19",d:24,e:"0.000000"},"11":{o:"content-box",h:"3",x:"visible",a:0,q:"100% 100%",b:0,j:"absolute",r:"none",c:495,k:"div",z:"11",d:370},"27":{o:"content-box",h:"19",x:"visible",a:280,q:"100% 100%",b:130,j:"absolute",r:"inline",c:14,k:"div",z:"27",d:13,e:"0.000000"},"12":{o:"content-box",h:"4",x:"visible",a:93,q:"100% 100%",b:33,j:"absolute",r:"inline",c:39,k:"div",z:"12",d:38,e:"0.000000"},"28":{o:"content-box",h:"20",x:"visible",a:267,q:"100% 100%",b:157,j:"absolute",r:"inline",c:14,k:"div",z:"28",d:17,e:"0.000000"},"20":{o:"content-box",h:"12",x:"visible",a:84,q:"100% 100%",b:163,j:"absolute",r:"inline",c:44,k:"div",z:"20",d:36,e:"0.000000"},"13":{o:"content-box",h:"5",x:"visible",a:133,q:"100% 100%",b:43,j:"absolute",r:"inline",c:35,k:"div",z:"13",d:35,e:"0.000000"},"29":{o:"content-box",h:"21",x:"visible",a:324,q:"100% 100%",b:136,j:"absolute",r:"none",c:27,k:"div",z:"29",d:27,e:"0.000000"},"21":{o:"content-box",h:"13",x:"visible",a:197,q:"100% 100%",b:157,j:"absolute",r:"inline",c:27,k:"div",z:"21",d:26,e:"0.000000"},"14":{o:"content-box",h:"6",x:"visible",a:183,q:"100% 100%",b:18,j:"absolute",r:"none",c:33,k:"div",z:"14",d:30,e:"0.000000"},"22":{o:"content-box",h:"14",x:"visible",a:206,q:"100% 100%",b:195,j:"absolute",r:"inline",c:19,k:"div",z:"22",d:22,e:"0.000000"},"15":{o:"content-box",h:"7",x:"visible",a:192,q:"100% 100%",b:68,j:"absolute",r:"inline",c:16,k:"div",z:"15",d:19,e:"0.000000"},"6":{o:"content-box",h:"0",x:"visible",a:225,q:"100% 100%",b:256,j:"absolute",r:"inline",c:24,k:"div",z:"8",d:168,e:"0.000000"},"30":{o:"content-box",h:"22",x:"visible",a:260,q:"100% 100%",b:180,j:"absolute",r:"none",c:19,k:"div",z:"30",d:22,e:"0.000000"},"23":{o:"content-box",h:"15",x:"visible",a:239,q:"100% 100%",b:35,j:"absolute",r:"inline",c:35,k:"div",z:"23",d:26,e:"0.000000"},"16":{o:"content-box",h:"8",x:"visible",a:118,q:"100% 100%",b:90,j:"absolute",r:"inline",c:25,k:"div",z:"16",d:24,e:"0.000000"},"7":{o:"content-box",h:"1",x:"visible",a:0,q:"100% 100%",b:0,j:"absolute",r:"inline",c:495,k:"div",z:"2",d:370},"8":{o:"content-box",h:"2",x:"visible",a:0,q:"100% 100%",b:0,j:"absolute",r:"inline",c:495,k:"div",z:"5",d:370,e:"0.000000"},"31":{o:"content-box",h:"23",x:"visible",a:289,q:"100% 100%",b:181,j:"absolute",r:"inline",c:18,k:"div",z:"31",d:15,e:"0.000000"},"24":{o:"content-box",h:"16",x:"visible",a:297,q:"100% 100%",b:27,j:"absolute",r:"inline",c:32,k:"div",z:"24",d:29,e:"0.000000"},"17":{o:"content-box",h:"9",x:"visible",a:153,q:"100% 100%",b:97,j:"absolute",r:"inline",c:27,k:"div",z:"17",d:25,e:"0.000000"}},n:"Untitled Scene",T:{kTimelineDefaultIdentifier:{d:3.2,i:"kTimelineDefaultIdentifier",n:"Main Timeline",a:[{f:"2",t:0,d:0.22,i:"e",e:"1.000000",s:"0.000000",o:"32"},{f:"2",t:0.15,d:1,i:"b",e:83,s:256,o:"6"},{f:"2",t:0.15,d:1,i:"a",e:225,s:225,o:"6"},{f:"2",t:0.15,d:1,i:"d",e:168,s:168,o:"6"},{f:"2",t:0.15,d:1,i:"c",e:24,s:24,o:"6"},{f:"2",t:0.15,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"8"},{f:"2",t:0.15,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"6"},{f:"2",t:1.15,d:1,i:"a",e:94,s:237,o:"10"},{f:"2",t:1.15,d:1,i:"c",e:143,s:0,o:"10"},{f:"2",t:1.15,d:1,i:"c",e:126,s:0,o:"9"},{f:"2",t:2,d:0.15,i:"b",e:40,s:40,o:"10"},{f:"2",t:2,d:0.15,i:"d",e:227,s:227,o:"10"},{f:"2",t:2,d:0.15,i:"a",e:237,s:237,o:"9"},{f:"2",t:2,d:0.15,i:"b",e:33,s:33,o:"9"},{f:"2",t:2,d:0.15,i:"d",e:227,s:227,o:"9"},{f:"2",t:2,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"22"},{f:"2",t:2,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"28"},{f:"2",t:2.01,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"21"},{f:"2",t:2.02,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"27"},{f:"2",t:2.04,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"19"},{f:"2",t:2.04,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"17"},{f:"2",t:2.05,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"30"},{f:"2",t:2.05,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"26"},{f:"2",t:2.07,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"31"},{f:"2",t:2.08,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"24"},{f:"2",t:2.08,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"16"},{f:"2",t:2.13,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"23"},{f:"2",t:2.14,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"18"},{f:"2",t:2.18,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"20"},{f:"2",t:2.2,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"29"},{f:"2",t:2.2,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"25"},{f:"2",t:2.28,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"15"},{f:"2",t:3.01,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"13"},{f:"2",t:3.05,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"12"},{f:"2",t:3.05,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"14"}],f:30}},o:"1"}];
	
	var javascripts = [];
	
	var functions = {};
	var javascriptMapping = {};
	for(var i = 0; i < javascripts.length; i++) {
		try {
			javascriptMapping[javascripts[i].identifier] = javascripts[i].name;
			eval("functions." + javascripts[i].name + " = " + javascripts[i].source);
		} catch (e) {
			hypeDoc.log(e);
			functions[javascripts[i].name] = (function () {});
		}
	}
	
	hypeDoc.setAttributeTransformerMapping(attributeTransformerMapping);
	hypeDoc.setResources(resources);
	hypeDoc.setScenes(scenes);
	hypeDoc.setJavascriptMapping(javascriptMapping);
	hypeDoc.functions = functions;
	hypeDoc.setCurrentSceneIndex(0);
	hypeDoc.setMainContentContainerID(mainContainerID);
	hypeDoc.setResourcesFolderName(resourcesFolderName);
	hypeDoc.setShowHypeBuiltWatermark(0);
	hypeDoc.setShowLoadingPage(false);
	hypeDoc.setDrawSceneBackgrounds(true);
	hypeDoc.setGraphicsAcceleration(true);
	hypeDoc.setDocumentName(documentName);

	HYPE.documents[documentName] = hypeDoc.API;
	document.getElementById(mainContainerID).setAttribute("HYPE_documentName", documentName);

	hypeDoc.documentLoad(this.body);
}());

