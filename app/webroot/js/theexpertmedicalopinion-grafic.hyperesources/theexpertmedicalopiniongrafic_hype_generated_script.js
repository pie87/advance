//	HYPE.documents["theexpertmedicalopinion-grafic"]

(function HYPE_DocumentLoader() {
	var resourcesFolderName = "theexpertmedicalopinion-grafic.hyperesources";
	var documentName = "theexpertmedicalopinion-grafic";
	var documentLoaderFilename = "theexpertmedicalopiniongrafic_hype_generated_script.js";
	var mainContainerID = "theexpertmedicalopiniongrafic_hype_container";

	// find the URL for this script's absolute path and set as the resourceFolderName
	try {
		var scripts = document.getElementsByTagName('script');
		for(var i = 0; i < scripts.length; i++) {
			var scriptSrc = scripts[i].src;
			if(scriptSrc != null && scriptSrc.indexOf(documentLoaderFilename) != -1) {
				resourcesFolderName = scriptSrc.substr(0, scriptSrc.lastIndexOf("/"));
				break;
			}
		}
	} catch(err) {	}

	// Legacy support
	if (typeof window.HYPE_DocumentsToLoad == "undefined") {
		window.HYPE_DocumentsToLoad = new Array();
	}
 
	// load HYPE.js if it hasn't been loaded yet
	if(typeof HYPE_160 == "undefined") {
		if(typeof window.HYPE_160_DocumentsToLoad == "undefined") {
			window.HYPE_160_DocumentsToLoad = new Array();
			window.HYPE_160_DocumentsToLoad.push(HYPE_DocumentLoader);

			var headElement = document.getElementsByTagName('head')[0];
			var scriptElement = document.createElement('script');
			scriptElement.type= 'text/javascript';
			scriptElement.src = resourcesFolderName + '/' + 'HYPE.js?hype_version=160';
			headElement.appendChild(scriptElement);
		} else {
			window.HYPE_160_DocumentsToLoad.push(HYPE_DocumentLoader);
		}
		return;
	}
	
	// handle attempting to load multiple times
	if(HYPE.documents[documentName] != null) {
		var index = 1;
		var originalDocumentName = documentName;
		do {
			documentName = "" + originalDocumentName + "-" + (index++);
		} while(HYPE.documents[documentName] != null);
		
		var allDivs = document.getElementsByTagName("div");
		var foundEligibleContainer = false;
		for(var i = 0; i < allDivs.length; i++) {
			if(allDivs[i].id == mainContainerID && allDivs[i].getAttribute("HYPE_documentName") == null) {
				var index = 1;
				var originalMainContainerID = mainContainerID;
				do {
					mainContainerID = "" + originalMainContainerID + "-" + (index++);
				} while(document.getElementById(mainContainerID) != null);
				
				allDivs[i].id = mainContainerID;
				foundEligibleContainer = true;
				break;
			}
		}
		
		if(foundEligibleContainer == false) {
			return;
		}
	}
	
	var hypeDoc = new HYPE_160();
	
	var attributeTransformerMapping = {b:"i",c:"i",bC:"i",d:"i",aS:"i",M:"i",e:"f",aT:"i",N:"i",f:"d",O:"i",g:"c",aU:"i",P:"i",Q:"i",aV:"i",R:"c",bG:"f",aW:"f",aI:"i",S:"i",bH:"d",l:"d",aX:"i",T:"i",m:"c",bI:"f",aJ:"i",n:"c",aK:"i",bJ:"f",X:"i",aL:"i",A:"c",aZ:"i",Y:"bM",B:"c",bK:"f",bL:"f",C:"c",D:"c",t:"i",E:"i",G:"c",bA:"c",a:"i",bB:"i"};
	
	var resources = {"10":{n:"txt6.jpg",p:1},"2":{n:"fons.jpg",p:1},"3":{n:"icon1.png",p:1},"11":{n:"icon2.png",p:1},"4":{n:"icon3.png",p:1},"5":{n:"icon5.png",p:1},"12":{n:"icon6.jpg",p:1},"6":{n:"txt2.jpg",p:1},"13":{n:"txt4.jpg",p:1},"7":{n:"txt3.jpg",p:1},"0":{n:"base.jpg",p:1},"8":{n:"txt5.jpg",p:1},"14":{n:"icon4.png",p:1},"1":{n:"pastilla-blanca.png",p:1},"9":{n:"txt1.jpg",p:1}};
	
	var scenes = [{x:0,p:"600px",c:"#FFFFFF",v:{"10":{o:"content-box",h:"11",x:"visible",a:211,q:"100% 100%",b:60,j:"absolute",r:"inline",c:163,k:"div",z:"4",d:106,e:"0.000000"},"2":{o:"content-box",h:"0",x:"visible",a:0,q:"100% 100%",b:0,j:"absolute",r:"none",c:990,k:"div",z:"2",d:478},"15":{o:"content-box",h:"6",x:"visible",a:206,q:"100% 100%",b:163,j:"absolute",r:"inline",c:178,k:"div",z:"9",d:116,e:"0.000000"},"9":{o:"content-box",h:"3",x:"visible",a:34,q:"100% 100%",b:59,j:"absolute",r:"inline",c:123,k:"div",z:"3",d:113,e:"0.000000"},"11":{o:"content-box",h:"4",x:"visible",a:410,q:"100% 100%",b:68,j:"absolute",r:"inline",c:169,k:"div",z:"5",d:104,e:"0.000000"},"16":{o:"content-box",h:"7",x:"visible",a:414,q:"100% 100%",b:172,j:"absolute",r:"inline",c:161,k:"div",z:"10",d:79,e:"0.000000"},"12":{o:"content-box",h:"14",x:"visible",a:621,q:"100% 100%",b:66,j:"absolute",r:"inline",c:145,k:"div",z:"6",d:104,e:"0.000000"},"17":{o:"content-box",h:"13",x:"visible",a:606,q:"100% 100%",b:171,j:"absolute",r:"inline",c:178,k:"div",z:"11",d:119,e:"0.000000"},"6":{o:"content-box",h:"1",x:"visible",a:-81,q:"100% 100%",b:0,j:"absolute",r:"inline",c:1241,k:"div",z:"15",d:477},"13":{o:"content-box",h:"5",x:"visible",a:822,q:"100% 100%",b:64,j:"absolute",r:"inline",c:146,k:"div",z:"7",d:113,e:"0.000000"},"7":{o:"content-box",h:"2",x:"visible",a:0,q:"100% 100%",b:-1,j:"absolute",r:"inline",c:990,k:"div",z:"1",d:478},"18":{o:"content-box",h:"8",x:"visible",a:807,q:"100% 100%",b:176,j:"absolute",r:"inline",c:176,k:"div",z:"12",d:166,e:"0.000000"},"14":{o:"content-box",h:"9",x:"visible",a:10,q:"100% 100%",b:163,j:"absolute",r:"inline",c:173,k:"div",z:"8",d:127,e:"0.000000"},"20":{o:"content-box",h:"10",x:"visible",a:445,q:"100% 100%",b:391,j:"absolute",r:"inline",c:311,k:"div",z:"14",d:40,e:"0.000000"},"19":{o:"content-box",h:"12",x:"visible",a:250,q:"100% 100%",b:374,j:"absolute",r:"inline",c:182,k:"div",z:"13",d:77,e:"0.000000"}},n:"Untitled Scene",T:{kTimelineDefaultIdentifier:{d:7,i:"kTimelineDefaultIdentifier",n:"Main Timeline",a:[{f:"2",t:0,d:0,i:"a",e:-81,s:-81,o:"6"},{f:"2",t:0,d:0.15,i:"a",e:150,s:-81,o:"6"},{f:"2",t:0.06,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"9"},{f:"2",t:0.15,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"14"},{f:"2",t:0.15,d:1,i:"a",e:150,s:150,o:"6"},{f:"2",t:1.15,d:0.15,i:"a",e:345,s:150,o:"6"},{f:"2",t:1.21,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"10"},{f:"2",t:1.21,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"19"},{f:"2",t:2,d:1,i:"a",e:345,s:345,o:"6"},{f:"2",t:2,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"15"},{f:"2",t:3,d:0.15,i:"a",e:546,s:345,o:"6"},{f:"2",t:3.06,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"11"},{f:"2",t:3.15,d:1,i:"a",e:546,s:546,o:"6"},{f:"2",t:3.15,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"16"},{f:"2",t:3.15,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"20"},{f:"2",t:4.15,d:0.15,i:"a",e:745,s:546,o:"6"},{f:"2",t:4.21,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"12"},{f:"2",t:5,d:1,i:"a",e:745,s:745,o:"6"},{f:"2",t:5,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"17"},{f:"2",t:6,d:0.15,i:"a",e:1006,s:745,o:"6"},{f:"2",t:6.06,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"13"},{f:"2",t:6.15,d:0.15,i:"e",e:"1.000000",s:"0.000000",o:"18"}],f:30}},o:"1"}];
	
	var javascripts = [];
	
	var functions = {};
	var javascriptMapping = {};
	for(var i = 0; i < javascripts.length; i++) {
		try {
			javascriptMapping[javascripts[i].identifier] = javascripts[i].name;
			eval("functions." + javascripts[i].name + " = " + javascripts[i].source);
		} catch (e) {
			hypeDoc.log(e);
			functions[javascripts[i].name] = (function () {});
		}
	}
	
	hypeDoc.setAttributeTransformerMapping(attributeTransformerMapping);
	hypeDoc.setResources(resources);
	hypeDoc.setScenes(scenes);
	hypeDoc.setJavascriptMapping(javascriptMapping);
	hypeDoc.functions = functions;
	hypeDoc.setCurrentSceneIndex(0);
	hypeDoc.setMainContentContainerID(mainContainerID);
	hypeDoc.setResourcesFolderName(resourcesFolderName);
	hypeDoc.setShowHypeBuiltWatermark(0);
	hypeDoc.setShowLoadingPage(false);
	hypeDoc.setDrawSceneBackgrounds(true);
	hypeDoc.setGraphicsAcceleration(true);
	hypeDoc.setDocumentName(documentName);

	HYPE.documents[documentName] = hypeDoc.API;
	document.getElementById(mainContainerID).setAttribute("HYPE_documentName", documentName);

	hypeDoc.documentLoad(this.body);
}());

