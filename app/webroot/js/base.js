$(document).ready(function() {

	/* * * ROLLOVERS * * */

	$('nav#menu ul li a, ul#submenu li a, nav#footermenu-left ul li a, nav#footermenu-right ul li a, nav#footer-submenu ul li a').hover(function () {
		$(this).animate({color: '#43acf0'}, 150, 'easeInQuad');
	}, function () {
		if($(this).hasClass('blue')) {

		} else {
			$(this).animate({color: '#6b6766'}, 150, 'easeOutQuad');
		}
	});

	$('nav#footermenu-left ul li a.turq, #employers-btn').hover(function () {
		$(this).animate({ color: '#43acf0'}, 150, 'easeInQuad');
	}, function () {
		$(this).animate({ color:'#007791' }, 150, 'easeOutQuad');
	});

	$(".home-box-btn, .faqs-btn, .about-box-btn, .employers-box-btn, .contact-btn").hover(function () {
		$(this).animate({ backgroundColor:'#002a54'}, 200, 'easeInQuad');
	}, function () {
		$(this).animate({ backgroundColor:'#007fd0'}, 200, 'easeInQuad');
	});

	$(".employers-btn, .know-btn").hover(function () {
		$(this).animate({ backgroundColor:'#002a54'}, 200, 'easeInQuad');
	}, function () {
		$(this).animate({ backgroundColor:'#007791'}, 200, 'easeInQuad');
	});

	$(".foremployers-txt p a, .careers-banner-subtitle a, .contact-banner-subtitle a").hover(function () {
		$(this).animate({ color:'#007fd0' }, 200);
	}, function () {
		$(this).animate({ color:'#002a54' }, 200);
	});

	$(".scrolltop-wrapp").hover( function () {
		$(".scrolltop-roll").fadeIn(200);
	}, function () {
		$(".scrolltop-roll").fadeOut(200);
	});


	/* ROLLOVERS INFO EXPERTS & PHYSICIANS */

	if(aqui == '/experts') {
		$(".def-hide").hover(function () {
			num = $(this).attr('rel');
			$(".expert-hand").hide();
			$(".exp-txt-def").hide(0, function () {
				$(".exp-txt"+num).show(0);	
			});
			src = '/img/experts/expert-'+num+'-roll.jpg';
			$(this).children('div').children('img').attr('src',src);
		}, function () {
			$(".exp-txt"+num).hide(0, function () {
				$(".expert-hand, .exp-txt-def").show(0);
			});	
			src = '/img/experts/expert-'+num+'.jpg';
			$(this).children('div').children('img').attr('src',src);
		});
	}

	if(aqui == '/physician-case-managers') {
		$(".def-hide").hover(function () {
			num = $(this).attr('rel');
			$(".expert-hand").hide();
			$(".exp-txt-def").hide(0, function () {
				$(".exp-txt"+num).show(0);	
			});
			src = '/img/experts/expert-phy-'+num+'-roll.jpg';
			$(this).children('div').children('img').attr('src',src);
		}, function () {
			$(".exp-txt"+num).hide(0, function () {
				$(".expert-hand, .exp-txt-def").show(0);
			});	
			src = '/img/experts/expert-phy-'+num+'.jpg';
			$(this).children('div').children('img').attr('src',src);
		});
	}

	/* END ROLLOVERS INFO EXPERTS & PHYSICIANS */

	
	/* BANNER TXT ANIMATIONS */

	$(".banner-head-right").delay(500).animate({ padding:'60px 17px 0px 15px',opacity:'1' }, 1000);
	$(".banner-head-right-small").delay(500).animate({ padding:'72px 17px 0px 10px',opacity:'1' }, 1000);
	$(".banner-head-right2").delay(500).animate({ padding:'50px 17px 0px 10px',opacity:'1' }, 1000);
	$(".banner-head-right3").delay(500).animate({ padding:'30px 17px 0px 0px',opacity:'1' }, 1000);
	$(".banner-global-right").delay(500).animate({ padding:'46px 17px 0px 0px',opacity:'1' }, 1000);
	$(".banner-contact-right").delay(500).animate({ padding:'88px 17px 0px 17px',opacity:'1' }, 1000);
	$(".careers-right").delay(500).animate({ padding:'45px 17px 0px 10px;',opacity:'1' }, 1000);
	$(".banner-employers-right-small").delay(500).animate({ padding:'128px 17px 0px 10px',opacity:'1' }, 1000);
	$(".banner-faqs-right-small").delay(500).animate({ padding:'63px 17px 0px 10px',opacity:'1' }, 1000);
	
	/* END BANNER TXT ANIMATIONS */


	/* * * END ROLLOVERS * * */

	/* * * ANIMATIONS * * */

	function checkElement() {

		if(aqui == '/global-offices') {
		    if($("#globalofficesmapa_hype_container").visible(true)) {
		    	var fileref=document.createElement('script');
				fileref.setAttribute("type","text/javascript");
				fileref.setAttribute("src", "/js/animations/global-offices-mapa.hyperesources/globalofficesmapa_hype_generated_script.js?54497");
				document.getElementsByTagName("head")[0].appendChild(fileref);
		    }
		}

		if(aqui == '/') {
		    if($("#animaciobox1_hype_container").visible(true)) {
		    	var fileref=document.createElement('script');
				fileref.setAttribute("type","text/javascript");
				fileref.setAttribute("src", "/js/animations/animacio-box1.hyperesources/animaciobox1_hype_generated_script.js?90455");
				document.getElementsByTagName("head")[0].appendChild(fileref);
		    }

		    if($("#minimapahome2_hype_container").visible(true)) {
		    	var fileref=document.createElement('script');
				fileref.setAttribute("type","text/javascript");
				fileref.setAttribute("src", "/js/animations/mini-mapa-home2.hyperesources/minimapahome2_hype_generated_script.js?40442");
				document.getElementsByTagName("head")[0].appendChild(fileref);
		    }
		}

		if(aqui == '/expert-medical-opinion') {
			 if($("#theexpertmedicalopiniongrafic_hype_container").visible(true)) {
		    	var fileref=document.createElement('script');
				fileref.setAttribute("type","text/javascript");
				fileref.setAttribute("src", "/js/theexpertmedicalopinion-grafic.hyperesources/theexpertmedicalopiniongrafic_hype_generated_script.js?60414");
				document.getElementsByTagName("head")[0].appendChild(fileref);
		    }

		    if($("#circles-box").visible(true)) {
				animateCircle();	
			}
		}

		if(aqui == '/why-advance') {
			if($("#why-box").visible(true)) {
				animateCircle();
			}
		}

	}

	checkElement();

	$(window).scroll(function() {

		checkElement();


	});

	


	/* * * END ANIMATIONS * * */

	/* * * RANDOM CIRCLES * * */

	setTimeout(function () { changecircle(); },3000);

	circle_actual = 0;
	circle_max = 3;

	txt_actual = 3;
	txt_max = 5;

	function changecircle() {
		if(circle_actual < circle_max) { circle_actual = circle_actual+1 } else { circle_actual = 1; }
		if(txt_actual < txt_max) { txt_actual = txt_actual+1 } else { txt_actual = 1; }
		html = $('.about-circle-txt-'+txt_actual).html();
		$('.about-circle-'+circle_actual).html(html);
		setTimeout(function () { changecircle(); },3000);
	}


	/* * * END RANDOM CIRCLES * * */

	/* * * SMOOTH SCROLLING * * */

	$(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});

	/* * * END SMOOTH SCROLLING * * */

	/* * * * SLIDER HOME * * * */

	actual=1;
	next=2;
	max=$("#slider img").length;
	time=1400;
	anim = 0;

	$("#img"+actual).css('margin-left','200px');

	repite=setTimeout(function () { nextslide(); },4000);

	function nextslide() {

		if (anim == 0) {

			anim = 1;
			clearTimeout(repite);
			if(actual<max) { next = actual+1; } else { next=1; }
			$("#slider-txt"+actual).fadeOut(600);
			$("#img"+actual).animate({'margin-left':'0px','opacity':'0'},time,'easeInOutQuart', function () {
				$("#slider-txt"+next).fadeIn(600);
			});

			$("#img"+next).delay(400).animate({'margin-left':'200px','opacity':'1'},time,'easeInOutQuart',function() {
				$("#img"+actual).animate({'margin-left':'400px'},1,function () {
					$("#img"+actual).animate({'z-index':'5'});
					$("#bot"+actual).removeClass('active');
					$("#bot"+next).addClass('active');
					actual=next;
					repite=setTimeout(function () { nextslide(); },4000);
					anim = 0;
				});
			});
		}
	}

	function prevslide() {
		clearTimeout(repite);
		if(actual>1) { next=actual-1; } else { next=max; }

    	$("#img"+actual).animate({'z-index':'4'},1);
    	$("#slider-txt"+actual).fadeOut(600);
    	$("#img"+next).animate({'margin-left':'0px'},1,function(){
    		$("#slider-txt"+next).fadeIn(600);
    		$("#img"+actual).animate({'margin-left':'200px'},time,'easeInOutQuart');
    		$("#img"+actual).fadeOut();
    		$("#img"+next).fadeIn();
    		$("#img"+next).animate({'margin-left':'0px'},time,'easeInOutQuart',function(){
        		$("#img"+actual).animate({'margin-left':'400px'},1,function(){
        			$("#img"+actual).animate({'z-index':'5'},1,function(){
        				$("#bot"+actual).removeClass('active');
						$("#bot"+next).addClass('active');
        				actual=next;
        				repite=setTimeout(function () { nextslide(); },4000);
        			});
        		});
        	});
    	});
	}

	
	
	/*
	$("#points ul li").on('click', function () {
		siguiente = $(this).attr('rel');
		console.log(siguiente);
		clearTimeout(repite);
		nextslide(siguiente);
		
	});
	*/




	/* * * * END SLIDER HOME * * * */

	/* * * * SLIDER EXPERT MEDICAL TXT * * * */

	if(aqui == '/experts') {

		txtactual=1;
		txtnext=2;
		txtmax=$(".opinion-slider").children("div.experts-slider-txt").length;
		txttime=1000;

		repitetxt=setTimeout(function () { next_txt(); },15000);

		function next_txt() {
			clearTimeout(repitetxt);
			if(txtactual<txtmax) { txtnext=txtactual+1; } else { txtnext=1; }
			
			$("#expert-img"+txtactual).fadeOut(txttime);
			$("#opslide-"+txtactual).fadeOut(txttime, 'easeInOutQuart', function () {
				$("#expert-img"+txtnext).fadeIn(txttime);
				$("#opslide-"+txtnext).fadeIn(txttime);
				$("#experts-bot"+txtactual).removeClass('active');
				$("#experts-bot"+txtnext).addClass('active');
				txtactual=txtnext;
				repitetxt=setTimeout(function () { next_txt(); },15000);
			});
			
		}
		
	}

	/* * * * SLIDER EXPERT MEDICAL TXT * * * */

	/* * * * SLIDER EXPERTS * * * */

	if(aqui == '/physician-case-managers') {

		txtactual2=1;
		txtnext2=2;
		txtmax2=$(".opinion-slider-wrapp").children("div").length;
		txttime=1000;


		repitetxt2=setTimeout(function () { next_txt2(); },4000);

		function next_txt2() {
			clearTimeout(repitetxt2);
			if(txtactual2<txtmax2) { txtnext2=txtactual2+1; } else { txtnext2=1; }
			
			$("#expert-img"+txtactual2).fadeOut(txttime);
			$("#opslide-"+txtactual2).fadeOut(txttime, 'easeInOutQuart', function () {
				$("#expert-img"+txtnext2).fadeIn(txttime);
				$("#opslide-"+txtnext2).fadeIn(txttime);
				$("#experts-bot"+txtactual2).removeClass('active');
				$("#experts-bot"+txtnext2).addClass('active');
				txtactual2=txtnext2;
				repitetxt2=setTimeout(function () { next_txt2(); },4000);
			});
			
		}

	}

	/* * * * END SLIDER EXPERTS* * * */

	

	/*animtxt = 0;

	$("#points ul li").on('click', function () {
		if(animtxt == 0) {
			animtxt = 1;
			clearTimeout(repite);
			num = $(this).attr('rel');
			next = num;

			$("#img"+actual).animate({'z-index':'4'},1);
			$("#slider-txt"+actual).fadeOut(600);
			$("#img"+actual).animate({'margin-left':'-2000px'},time,'easeInOutQuart', function () {
				$("#img"+actual).fadeOut();
				$("#slider-txt"+next).fadeIn(600);
			});
			$("#img"+next).fadeIn();
			$("#img"+next).animate({'margin-left':'-205px'},time,'easeInOutQuart',function() {
				$("#img"+actual).animate({'margin-left':'2000px'},1,function () {
					$("#img"+actual).animate({'z-index':'5'});
					$("#bot"+actual).removeClass('active');
					$("#bot"+next).addClass('active');
					actual=next;
					repite=setTimeout(function () { nextslide(); },4000);
					anim = 0;
				});
			});
		}
	});*/



	/* * * * END SLIDER HOME * * * */

	$(".faqs-subtitle").on('click', function () {
		if($(this).next("div.faqs-txt").hasClass('closed')) {
			$(this).next("div.faqs-txt").slideDown(300, 'easeOutCubic', function () {
				$(this).removeClass('closed');
				$(this).addClass('opened');
			});
		} else if($(this).next("div.faqs-txt").hasClass('opened')) {
			$(this).next("div.faqs-txt").slideUp(300, 'easeOutCubic', function () {
				$(this).removeClass('opened');
				$(this).addClass('closed');
			});
		}
	});





});

function animateCircle() {
	$(function() {
	    $('.ch1, .ch2, .ch3, .ch4').easyPieChart({
	    	animate:2000,
	    	lineWidth:6,
	    	size:186,
	    	trackColor:false,
	    	barColor: '#0c85d4',
	    	scaleColor:false,
	    	lineCap: 'square',
	    });
	});
}

/*
function randomnum() {
	return Math.floor(Math.random() * (6 - 1) + 1);
}
*/

