<div class="content-wrapp blue-degree-bg">
	<div class="wrapp">

		<div class="banner-head">
			<div class="banner-head-left">
				<img src="/img/img-theexpertmedicalopinion.png" />
			</div>
			<div class="banner-head-right">
				<p>
					From the comfort of your own home,<br/>
					you can ask the world’s finest<br/>
					medical minds for their opinions<br>
					about your health status.
				</p>
			</div>
		</div>

	</div>
</div>

<div class="content-wrapp header-bg">
	<div class="wrapp">
		<div class="submenu-wrapp"></div>
	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">


		<div id="expert-infofaq">

			<div id="expert-infofaq-txt" class="blue">
				<span class="dark-blue">The Expert Medical Opinion program</span> gives you access to world renowned doctors who are chosen specifically for you. These experts provide a review and recommendations about your diagnosis and treatment plan. 
			</div>
			<div id="moreinfo-faq">
				<div id="moreinfo-faq-title" class="dark-blue">
					MORE INFORMATION
				</div>
				<div class="faqs-btn" onClick="window.location='/faqs'">
					<div class="faqs-btn-txt">
						FAQS
					</div>
				</div>
			</div>

		</div>

		<div class="expert-info-title dark-blue">
			EXPERT MEDICAL OPINION
		</div>
		<div class="expert-info-title-txt">
			<div class="expert-info-title-txt-left dark-grey">

					This program gives you the unique opportunity to have the world’s leading physician experts review your medical situation to ensure that your care reflects the absolute best in medicine. We work with national and international specialists to provide you with a comprehensive report about your medical condition. During the process, you will be personally supported by your dedicated Physician Case Manager, who will review your medical history and oversee every aspect of your case. He/she will give you regular updates about the status of your case and be available to discuss changes in your medical condition. In a short amount of time and without needing to travel, you will find out exactly what the world’s leading experts think about your medical condition, allowing you make confident decisions about next steps.

			</div>
			
		</div>
		

	</div>
</div>

<div class="content-wrapp blue-title-bg">
	<div class="wrapp">

		<div class="blue-title-left">
			WHEN IS IT APPROPIATE?
		</div>

		<div class="blue-title-right dark-blue">
			Expert Medical Opinion is for anyone seeking information for clarity and confidence about any health-related decision. 
		</div>

	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">

		<div class="expert-info-title dark-blue">
			HOW DOES THE PROCESS WORK?
		</div>
		<div id="process-work">
			<div id="theexpertmedicalopiniongrafic_hype_container" style="position:relative;overflow:hidden;width:990px;height:478px;">
				
			</div>
		</div>

	</div>
</div>

<div class="content-wrapp blue-title-bg">
	<div class="wrapp">

		<div id="expertopinion-box">

			<div id="expert-opinion-img">	
				<img src="/img/foto-theexpertmedicalopinion-cita.png" />
			</div>

			<div class="expertopinion-title">
				WHY GET AN EXPERT MEDICAL OPINION?
			</div>
			<div class="expertopinion-subtitle dark-blue">
				...because it can change absolutely everything
			</div>
			<div class="opinion-slider-wrapp">
				<div class="opinion-slider">
					<div id="opslide" class="oslide">
						<div class="opinion-slider-txt">
							“It’s like having your own personalized physician—personalized service. My Physician Case Manager was very proactive and great
							at keeping in contact. I needed to know all the options were explored before going forward with my surgery, and Expert Medical
							Opinion gave me that peace of mind.”

						</div>
						<div class="opinion-slider-subtxt dark-blue">
							<strong>JESSICA</strong>, LOS ANGELES
						</div>
					</div>
				</div>
			</div>
			<!--
			<div id="opinion-points">
				<ul>
					<li id="opinion-bot1" class="active"></li>
					<li id="opinion-bot2"></li>
				</ul>
			</div>
			-->

		</div>

	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">

		<div class="expert-info-title dark-blue">
			NUMBERS OUR PATIENTS CARE ABOUT
		</div>

		<div id="circles-box">

			<div class="contact-box-content cb1">
				<div class="circle-icon">
					<img src="/img/icon-circle1.jpg" />
				</div>
				<div class="circle-wrapp">
					<div class="circle1">
						<div class="chart ch1 percent2 blue-sea" data-percent="7">7</div>
						<div class="contact-box circle"></div>
					</div>
				</div>
				<div class="percent-txt blue-sea"> 
					Experts make a major change to the original diagnosis
				</div>
				<div class="circle-shadow"></div>
			</div>
			

			<div class="contact-box-content cb2">
				<div class="circle-icon">
					<img src="/img/icon-circle2.jpg" />
				</div>
				<div class="circle-wrapp">
					<div class="circle2">
						<div class="chart ch2 percent blue-sea" data-percent="21">21</div>
						<div class="contact-box circle"></div>
					</div>
				</div>
				<div class="percent-txt blue-sea"> 
					Experts recommend 
					a major change in treatment.
				</div>
				<div class="circle-shadow"></div>
			</div>

			<div class="contact-box-content cb3">
				<div class="circle-icon">
					<img src="/img/icon-circle3.jpg" />
				</div>
				<div class="circle-wrapp">
					<div class="circle3">
						<div class="chart ch3 percent blue-sea" data-percent="20">20</div>
						<div class="contact-box circle"></div>
					</div>
				</div>
				<div class="percent-txt blue-sea"> 
					Patients report<br/>
					avoiding surgery
				</div>
				<div class="circle-shadow"></div>
			</div>

			<div class="contact-box-content cb4">
				<div class="circle-icon">
					<img src="/img/icon-circle4.jpg" />
				</div>
				<div class="circle-wrapp">
					<div class="circle4">
						<div class="chart ch4 percent blue-sea" data-percent="98">99</div>
						<div class="contact-box circle"></div>
					</div>
				</div>
				<div class="percent-txt blue-sea"> 
					Patients would<br/>
					recommend us
				</div>
				<div class="circle-shadow"></div>
			</div>


		</div>

		<div class="circles-subtitle blue-sea">
			
		</div>

	</div>
</div>
