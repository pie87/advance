<div class="content-wrapp blue-degree-bg">
	<div class="wrapp">

		<div class="banner-head">

			<div class="banner-head-left">
				<img src="/img/img-global-offices.png" />
			</div>
			
			<div class="banner-global-right">
				<p>
					Advance Medical provides coverage<br/>
					to more than 25 million patients<br/>
					around the world from offices in<br/>
					Boston, Barcelona, Budapest,<br/>
					São Paulo and Santiago.
				</p>
				<div class="banner-head-name blue">
					
				</div>
			</div>
		</div>

	</div>
</div>

<div class="content-wrapp header-bg">
	<div class="wrapp">
		<div class="submenu-wrapp"></div>
	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">

		<div id="global-infobox2">

			<div class="global-info-title dark-blue">
				OUR GLOBAL REACH
			</div>

			<div id="global-map">
				<div id="globalofficesmapa_hype_container" style="position:relative;overflow:hidden;width:990px;height:520px;">
					<!-- Load js file from base.js -->	
				</div>
			</div>

			<div class="country-list">
				<div class="country-list-title blue-sea">
					PATIENT SERVICE<BR/>OFFICES
				</div>
				<div class="country-list-title-sep"></div>
				<ul>
					<li>BOSTON <span class="country-list-grey">USA</span></li>
					<li>BARCELONA <span class="country-list-grey">SPAIN</span></li>
					<li>SãO PAULO <span class="country-list-grey">BRAZIL</span></li>
					<li>SANTIAGO <span class="country-list-grey">CHILE</span></li>
					<li>BUDAPEST <span class="country-list-grey">HUNGARY</span></li>
				</ul>
			</div>

			<div class="country-list" style="width:230px;">
				<div class="country-list-title blue-sea">
					LOCAL<br/>EXPERTISE
				</div>
				<div class="country-list-title-sep"></div>
				<ul>
					<li>BRISBANE <span class="country-list-grey">AUSTRALIA</span></li>
					<li>OTTAWA <span class="country-list-grey">CANADA</span></li>
					<li>SHANGHAI <span class="country-list-grey">CHINA</span></li>
					<li>DUBAI <span class="country-list-grey">UAE</span></li>
					<li>PARIS <span class="country-list-grey">FRANCE</span></li>
					<li>BERLIN <span class="country-list-grey">GERMANY</span></li>
				</ul>
			</div>

			<div class="country-list" style="width:230px;margin-top: 79px;">
				<ul>
					<li>ATHENS <span class="country-list-grey">GREECE</span></li>
					<li>DELHI <span class="country-list-grey">INDIA</span></li>
					<li>NAIROBI <span class="country-list-grey">KENYA</span></li>
					<li>KUALA LUMPUR <span class="country-list-grey">MALAYSIA</span></li>
					<li>MEXICO CITY <span class="country-list-grey">MEXICO</span></li>
					<li>WARSAW <span class="country-list-grey">POLAND</span></li>
				</ul>
			</div>

			<div class="country-list" style="width:200px;margin-top: 79px;">
				<ul>
					<li>LISBON <span class="country-list-grey">PORTUGAL</span></li>
					<li>STOCKHOLM <span class="country-list-grey">SWEDEN</span></li>
					<li>TOKYO <span class="country-list-grey">JAPAN</span></li>
					<li>LONDON <span class="country-list-grey">UK</span></li>
					<li>DALLAS <span class="country-list-grey">USA</span></li>
					<li>CHARLOTTE <span class="country-list-grey">USA</span></li>
				</ul>
			</div>

		</div>

	</div>
</div>

