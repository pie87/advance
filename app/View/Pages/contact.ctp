<div class="content-wrapp blue-degree-bg">
	<div class="wrapp">

		<div class="banner-head">

			<div class="banner-head-left">
				<img src="/img/contact-banner-laptop.png" />
			</div>
			
			<div class="banner-contact-right">
				<p>
					Turning complexity and uncertainty…<br/>
					<span class="blue">into confidence and trust.</span>
				</p>
				<div class="banner-head-name blue">
					
				</div>
			</div>
		</div>

	</div>
</div>
<div class="content-wrapp header-bg">
	<div class="wrapp">
		<div class="submenu-wrapp"></div>
	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">

		<div id="global-infobox">

			<div class="expert-info-title dark-blue">
				CONTACT US
			</div>

		

			<div class="contact-list">
				<div class="contact-list-icon">
					<img src="/img/icon-america.jpg" />
				</div>
				<div class="contact-list-title blue-sea">
					NORTH<br/>AMERICA
				</div>
				<div class="contact-list-title-sep"></div>
				<ul>
					<li>BOSTON <span class="country-list-grey">USA</span></li>
					<li class="info">100 Lowder Brook</li>
					<li class="info">Westwood MA 02090</li>
      				<li class="info telf">8663421987</li>
      				<li class="info mail" onClick="window.location='mailto:contact@advance-medical.com'"></li>
      				<li class="info map" onClick="window.open('https://maps.google.es/maps?q=100+Lowder+Brook+Westwood+MA+02090&hl=es&sll=41.39479,2.148768&sspn=0.261156,0.490265&hnear=100+Lowder+Brook+Dr,+Westwood,+Massachusetts+02090,+Estados+Unidos&t=m&z=17&iwloc=A')"></li>
				</ul>
			</div>

			<div class="contact-list" style="width:530px;">
				<div class="contact-list-icon">
					<img src="/img/icon-south-america.jpg" />
				</div>
				<div class="contact-list-title blue-sea">
					SOUTH<br/>AMERICA
				</div>
				<div class="contact-list-title-sep2"></div>
				<ul>
					<li class="short">Santiago de Chile <span class="country-list-grey">Chile</span></li>
					<li class="short info">Avda. Lota 2267 Of. 402</li>
      				<li class="short info telf">+00 56 800 633 426</li>
      				<li class="info mail" onClick="window.location='mailto:contact@advance-medical.com'"></li>
      				<li class="info map" onClick="window.open('https://maps.google.com/maps?q=Calle+Lota+2267,+Of+402,+Providencia,+santiago&hl=es&ie=UTF8&sll=-23.567655,-46.650449&sspn=0.003383,0.006035&hq=Calle+Lota+2267,+Of+402,+Providencia,&hnear=Santiago,+Regi%C3%B3n+Metropolitana+de+Santiago+de+Chile,+Chile&t=m&z=15&iwloc=A')"></li>
				</ul>
				<ul>
					<li class="short">São Paulo <span class="country-list-grey">Brasil</span></li>
					<li class="short info">Av. Brig Luis Antonio, 2504</li>
					<li class="short info">Cj 202 01.402-000</li>
      				<li class="short info telf">+00 55 11 3504-4900</li>
      				<li class="info mail" onClick="window.location='mailto:contact@advance-medical.com'"></li>
      				<li class="info map" onClick="window.open('https://maps.google.com/maps?f=q&source=s_q&hl=es&geocode=&q=Rua+Brig+Lu%C3%ADs+Antônio,+2504,+Estado+de+São+Paulo,+Brasil&aq=0&oq=Av.+Brig+Luis+Antonio,+2504&sll=37.0625,-95.677068&sspn=54.79724,119.091797&vpsrc=6&ie=UTF8&hq=Rua+Brig+Lu%C3%ADs+Antônio,+2504,+Estado+de&hnear=São+Paulo+-+Estado+de+São+Paulo,+Brasil&ll=-23.567552,-46.650449&spn=0.003919,0.007269&t=m&z=18&cid=9130063855094968170&iwloc=A')"></li>
				</ul>
				
			</div>

			<div class="contact-list">
				<div class="contact-list-icon">
					<img src="/img/icon-europe.jpg" />
				</div>
				<div class="contact-list-title blue-sea">
					WESTERN EUROPE<br/>AND ASIA
				</div>
				<div class="contact-list-title-sep"></div>
				<ul>
					<li>BARCELONA <span class="country-list-grey">SPAIN</span></li>
					<li class="short info">Moragas, 11</li>
					<li class="short info">08022</li>
      				<li class="short info telf">+00 34 902 158 530</li>
      				<li class="info mail" onClick="window.location='mailto:contact@advance-medical.com'"></li>
      				<li class="info map" onClick="window.open('https://maps.google.es/maps?q=Carrer+Moragas,+11,+Barcelona&hl=es&sll=41.414626,2.128024&sspn=0.261077,0.490265&oq=carrer+Moragas,11&hnear=Carrer+Moragas,+11,+08022+Barcelona&t=m&z=17&iwloc=A')"></li>
				</ul>
			</div>

			<div class="contact-list">
				<div class="contact-list-icon">
					<img src="/img/icon-central-europe.jpg" />
				</div>
				<div class="contact-list-title blue-sea">
					CENTRAL &amp;<br/>EASTERN EUROPE
				</div>
				<div class="contact-list-title-sep"></div>
				<ul>
					<li>BUDAPEST <span class="country-list-grey">HUNGARY</span></li>
					<li class="short info">1085 Budapest</li>
					<li class="short info">Baross u. 22</li>
      				<li class="short info telf">+00 36 1 416 15 15</li>
      				<li class="info mail" onClick="window.location='mailto:contact@advance-medical.com'"></li>
      				<li class="info map" onClick="window.open('https://maps.google.es/maps?q=1085+Budapest,+VIII.+ker%C3%BClet,+Baross+utca+22,+Hungr%C3%ADa&hl=es&ie=UTF8&sll=41.408115,2.13665&sspn=0.008159,0.015321&geocode=FaKg1AIdeewiAQ&hnear=1085+Budapest,+VIII.+ker%C3%BClet,+Baross+utca+22,+Hungr%C3%ADa&t=m&z=17&iwloc=A')"></li>
				</ul>
			</div>

			

			
		</div>

		<div id="contact-form">

			<div class="expert-info-title dark-blue">
				SEND US A MESSAGE
			</div>

			<div class="contact-banner-subtitle blue-sea">
				For more information about our Expert Medical Opinion program,<br/>
				please email us: <a class="dark-blue" href="mailto:contact@advance-medical.com">contact@advance-medical.com</a>
			</div>

			<!--
			<?php 
				/*echo $this->Form->create('contact', array('id'=>'contactform')); 
				echo $this->Form->input('name', array('class'=>'input-contact','value'=>'Write your Name','div'=>'input-wrapp-left','label'=>array('class'=>'label-contact','text'=>'Name*')));
				echo $this->Form->input('email', array('class'=>'input-contact','value'=>'Write your Email','div'=>'input-wrapp-right','label'=>array('class'=>'label-contact','text'=>'Email*')));
				echo $this->Form->input('country', array('class'=>'input-contact input-country','value'=>'Write your Country Code','div'=>'input-wrapp-left','label'=>array('class'=>'label-contact','text'=>'Country code*')));
				echo $this->Form->input('phone', array('class'=>'input-contact','value'=>'Write your Phone','div'=>'input-wrapp-right','label'=>array('class'=>'label-contact','text'=>'Phone*')));
				echo $this->Form->input('message', array('type'=>'textarea','class'=>'input-textarea', 'value'=>'Write your Message','div'=>'input-wrapp-left','label'=>array('class'=>'label-contact','text'=>'Message')));
				echo $this->Form->input('privacy', array('type'=>'checkbox','class'=>'checkbox-contact','div'=>'checkbox-wrapp','label'=>array('text'=>'I accept the terms outlined in the <span class="blue-sea">Privacy Notice*.</span>','class'=>'checkbox-label')));*/
			?>
			<div class="contact-btn">
				<div class="faqs-btn-txt">
					SEND
				</div>
			</div>

			<div class="contact-form-subtxt dark-grey">
				<p style="font-size:12px;">*Required.</p><br/>

				<p>
					By using this Advance Medical webform, you are accepting the terms described in our Privacy Notice. Any personal data received by Advance Medical will remain private property and will not be distributed externally.
				</p>
			</div>

			-->



		</div>

	</div>
</div>

