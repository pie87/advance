<div class="content-wrapp blue-degree-bg">
	<div class="wrapp">

		<div class="banner-head">
			<div class="banner-head-left">
				<img class="banner-head-left-ab" src="/img/img-about-us.png" />
			</div>
			<div class="banner-head-right-small">
				<p>
					Bringing the<br/>
					world’s medical experts<br/>
					within arm’s reach.
				</p>
			</div>
		</div>

	</div>
</div>

<div class="content-wrapp header-bg">
	<div class="wrapp">
		<div class="submenu-wrapp"></div>
	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">

		<div class="aboutus-info-title dark-blue">
			ABOUT US
		</div>
		<div class="aboutus-info-txt blue-sea">
			Advance Medical is an international health care services provider<br/>
			that has been helping patients with expert consultations from<br/>
			global experts since 1999.
		</div>
		<div class="aboutus-info-title-txt">
			<!--<div class="expert-info-title-txt-left dark-grey">
				<p>
					Our more than 500 employees – nearly half of whom are physicians - collaborate with the world’s best medical resources to bring patients optimal solutions for their illnesses. We provide our services through offices in Boston, Barcelona, Santiago, Budapest, and
				</p>
			</div>
			<div class="expert-info-title-txt-right dark-grey">
				<p>
					   
				</p>
			</div> -->
			<div class="expert-info-title-txt-center dark-grey">
				<p>
					Our more than 500 employees – nearly half of whom are physicians - collaborate with the world’s best medical resources to bring patients optimal solutions for their illnesses. We provide our services through offices in Boston, Barcelona, Santiago, Budapest, and São Paulo, as well as through Advance Medical physicians based in many other countries. Over 25 million patients globally already enjoy access to our Expert Medical Opinion program through coverage provided by their insurance plans or employers.
				</p>
			</div>
		</div>

		<div class="about-box-btn" onClick="window.location='/global-offices'">
			<div class="home-box-btn-txt">
				OUR GLOBAL OFFICES
			</div>
		</div>
		

	</div>
</div>

<div class="content-wrapp blue-title-bg">
	<div class="wrapp">

		<div id="about-numbers">
			<div id="about-numbers-title">
				IN NUMBERS...
			</div>
			<div class="about-numbers-circle about-circle-1">
				<div class="about-numbers-circle-txt">
					<span class="dark-blue"><b>1,000,000+</b></span><br/>
					calls every year<br/>
					from <span class="dark-blue"><b>100+</b></span><br/>
					countries
				</div>
			</div>
			<div class="about-numbers-circle about-circle-2">
				<div class="about-numbers-circle-txt2">
					<span class="dark-blue"><b>500+</b></span><br/>
					Employees
				</div>
			</div>
			<div class="about-numbers-circle about-circle-3">
				<div class="about-numbers-circle-txt">
					20+ <span class="dark-blue"><b>Native<br/>
					Languages<br/>
					Spoken By<br/>
					Physicians</span></b>
				</div>
			</div>
		</div>

		<div class="about-circle-txt-1 ac-hide">
			<div class="about-numbers-circle-txt">
				<span class="dark-blue"><b>1,000,000+</b></span><br/>
				calls every year<br/>
				from <span class="dark-blue"><b>100+</b></span><br/>
				countries
			</div>
		</div>

		<div class="about-circle-txt-2 ac-hide">
			<div class="about-numbers-circle-txt2">
				<span class="dark-blue"><b>500+</b></span><br/>
				Employees
			</div>
		</div>

		<div class="about-circle-txt-3 ac-hide">
			<div class="about-numbers-circle-txt">
				20+ <span class="dark-blue"><b>Native<br/>
				Languages<br/>
				Spoken By<br/>
				Physicians</span></b>
			</div>
		</div>

		<div class="about-circle-txt-4 ac-hide">
		
			<div class="about-numbers-circle-txt2">
				<span class="dark-blue"><b>250+</b></span><br/>
				Physicians
			</div>
			
		</div>

		<div class="about-circle-txt-5 ac-hide">
		
			<div class="about-numbers-circle-txt">
				<span class="dark-blue"><b>Covering 25,000,000+</b></span><br/>
				lives worldwide<br/>
			</div>
			
		</div>

	</div>
</div>


<div class="content-wrapp">
	<div class="wrapp">

		<div class="aboutus-info-title dark-blue">
			INFORMATION FOR EMPLOYERS
		</div>
		<div class="foremployers-txt dark-grey">
			<p>Advance Medical provides employers with the opportunity to offer concierge-level medical support<br/>
			for their employees through our Expert Medical Opinion program. </p>

			<p>For more information about these services, contact<br/>
			<a class="dark-blue" href="mailto:businessdevelopment@advance-medical.com">businessdevelopment@advance-medical.com</a></p>
		</div>

		<div class="employers-box-btn2" onClick="window.location='/employers'">
			<div class="home-box-btn-txt">
				READ MORE
			</div>
		</div>

	</div>
</div>

