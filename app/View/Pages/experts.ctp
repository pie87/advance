<div class="content-wrapp blue-degree-bg">
	<div class="wrapp">

		<div class="banner-head">
			<div class="banner-head-left">
				<img src="/img/img-experts.png" />
			</div>
			<div class="banner-head-right-small">
				<p>
					“Imagine the difference it makes<br/>
					when more than one world class expert<br/>
					gives you the same advice.”
				</p>
				<div class="banner-head-name blue">
					<b>JUAN,</b> CHICAGO 
				</div>
			</div>
		</div>

	</div>
</div>

<div class="content-wrapp header-bg">
	<div class="wrapp">
		<div class="submenu-wrapp"></div>
	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">

		<div id="experts-infobox">

			<div class="expert-info-title dark-blue">
				UNRESTRICTED EXPERT POOL
			</div>

			<div class="experts-banner-subtitle dark-blue">
				We work with physician experts from all parts of the globe.<br/>
				<span class="blue-sea">Because we form relationships with individual doctors and do not<br/>
				restrict ourselves to particular hospital networks, we can always find<br/>
				 the most appropriate experts for your condition.</span>
			</div>

		</div>

		<div class="expert-info-title dark-blue">
			HOW DO WE CHOOSE OUR EXPERTS?
		</div>
		
		<div class="experts-banner-subtitle2 dark-grey">
			We place incredible value on the importance of this decision. For each case, we assemble a Clinical Committee (made up of several doctors) and carefully consider several factors when choosing our experts.
		</div>

		<div class="expertschoose-banner-subtitle blue-sea">
			Our number one priority is always the relevance of the<br/>
			expert’s experience to your particular condition.
		</div>
		
		<div class="expert-info-title-txt-blue">
			<div class="expert-info-title-txt-left dark-grey">
				<p>
					We look for experts who are recognized within the medical community as thought-leaders in specific diseases. Although there are many skilled specialists, we focus on finding experts who are working on the cutting edge – those aware of the latest research and upcoming clinical trials. These experts often hold significant positions at academic institutions, major medical centers and journals. However, we ultimately choose them for their disease-specific knowledge and experience successfully treating patients.
				</p>
			</div>
		
		</div>

		<div class="expert-info-title dark-blue">
			EXAMPLES OF OUR EXPERTS
		</div>

		<div id="experts-people">
			<div id="expert1" class="def-hide" rel="1">
				<div class="expert-people-avatar">
					<img src="/img/experts/expert-1.jpg" />
				</div>
				<div class="expert-people-avatar-name dark-grey">
					Donald I. Abrams, M.D.
				</div>
			</div>

			<div id="expert2" class="def-hide" rel="2">
				<div class="expert-people-avatar">
					<img src="/img/experts/expert-2.jpg" />
				</div>
				<div class="expert-people-avatar-name dark-grey">
					Franco M. Muggia, M.D.
				</div>
			</div>

			<div id="expert3" class="def-hide" style="margin-left:375px;" rel="3">
				<div class="expert-people-avatar">
					<img src="/img/experts/expert-3.jpg" />
				</div>
				<div class="expert-people-avatar-name dark-grey">
					Theodore I. Steinman, M.D.
				</div>
			</div>

			<!--<div id="expert4">
				<div class="expert-people-avatar">
					
				</div>
				<div class="expert-people-avatar-name dark-grey">
					Elliot S. Krames, MD
				</div>
			</div>-->

			<div id="expert5" class="def-hide" rel="4">
				<div class="expert-people-avatar">
					<img src="/img/experts/expert-4.jpg" />
				</div>
				<div class="expert-people-avatar-name dark-grey">
					Elliot S. Krames, M.D.
				</div>
			</div>

			<div id="expert6" class="def-hide" rel="5">
				<div class="expert-people-avatar">
					<img src="/img/experts/expert-5.jpg" />
				</div>
				<div class="expert-people-avatar-name dark-grey">
					Janice F. Wiesman, M.D.
				</div>
			</div>

			<div id="experts-people-box" class="blue-sea">
				<div class="expert-hand"></div>
				<div class="experts-people-box-txt exp-txt-def">
					<?php if($this->request->is('mobile')){ echo "Tap on "; }else{ echo "Roll over ";} ?>images to<br/>
					learn more about our<br>
					Experts
				</div>
				<div class="experts-people-box-txt exp-txt1 exp-txt-roll">
					Professor of Medicine, University of<br/>
					California San Francisco<br/>
					Chief, Division of Hematology and<br/>
					Oncology, San Francisco General Hospital<br/>
					Integrative Oncology, UCSF Osher Center for Integrative Medicine<br/>
					<span class="dark-blue">San Francisco, CA</span>
				</div>
				<div class="experts-people-box-txt exp-txt2 exp-txt-roll">
					Professor of Medicine (Oncology),
					Breast Cancer Program, Gynecologic Cancer, Division of Hematology/Medical Oncology,
					Perlmutter Cancer Institute at Langone Medical Center and NYU School of Medicine
					<span class="dark-blue">New York, NY, USA</span>
				</div>
				<div class="experts-people-box-txt exp-txt3 exp-txt-roll" style="height: 128px;padding: 32px 20px 0px 20px;">
					Clinical Professor of Medicine,
					Harvard Medical School;
					Senior Physician, Beth Israel Deaconess Medical Center and Brigham & Women's Hospital
					<span class="dark-blue">Boston, MA</span>
				</div>

				<div class="experts-people-box-txt exp-txt4 exp-txt-roll" style="height: 128px;padding: 32px 20px 0px 20px;">
					Board Member Emeritus, International Neuromodulation Society (INS); Editor in Chief, Emeritus, Neuromodulation:
					Journal of the INS
					<span class="dark-blue">San Francisco, CA</span>
				</div>
				<div class="experts-people-box-txt exp-txt5 exp-txt-roll" style="height: 110px;padding: 50px 20px 0px 20px;">
					Adjunct Assistant Professor of Neurology Boston University School of Medicine
					<span class="dark-blue">Boston, USA</span>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="content-wrapp blue-title-bg">
	<div class="wrapp">

		<div id="experts-banner-box">

			<div id="expert-banner-img">
				<img id="expert-img1" src="/img/experts-cita-foto1.png" />
				<img id="expert-img2" class="hideimg" src="/img/experts-cita-foto2.png" />
				<img id="expert-img3" class="hideimg" src="/img/experts-cita-foto3.png" />
			</div>

			<div class="opinion-slider-wrapp">
				<div id="opslide-1" class="opinion-slider">
					<div class="experts-slider-txt">
						"When you have concerns about your diagnosis, your prognosis, or the range of treatment options available to you - that is the time to ask for more information. An Expert Second Opinion provides these checks and balances of all the medical information that impacts your care." 
					</div>
					<div class="experts-slider-subtxt dark-blue">
						<strong>Theodore I. Steinman, M.D.</strong><br/>
						Clinical Professor of Medicine, Harvard Medical School;<br/>
						Senior Physician, Beth Israel Deaconess Medical Center and Brigham &amp; Women's Hospital<br/>
						Boston, MA
					</div>
				</div>
				<div id="opslide-2" class="opinion-slider hideimg">
					<div class="experts-slider-txt">
						“Advances in neurology occur at a fast pace. Advance Medical provides access to specialists who have the luxury of time to spend reviewing a person’s medical record and the current medical literature in order to provide information regarding treatment options and the future course of the illness.“
					</div>
					<div class="experts-slider-subtxt dark-blue">
						<strong>Janice F. Wiesman, M.D.</strong><br/>
						Adjunct Assistant Professor of Neurology Boston University School of Medicine Boston, USA 
					</div>
				</div>
				<div id="opslide-3" class="opinion-slider hideimg">
					<div class="experts-slider-txt">
						 “Having been involved in this activity for 12 years, I confidently conclude that the Expert Medical Opinion program is of unique value to oncology patients that become preoccupied with their diagnosis and treatment.” 
					</div>
					<div class="experts-slider-subtxt dark-blue">
						<strong>Franco M. Muggia, M.D.</strong><br/>
						Professor of Medicine (Oncology),
						Breast Cancer Program, Gynecologic Cancer, Division of Hematology/Medical Oncology,
						Perlmutter Cancer Institute at Langone Medical Center and NYU School of Medicine
						New York, NY, USA 


					</div>
				</div>
			</div>
			
			<div id="experts-points">
				<ul>
					<li id="experts-bot1" class="active"></li>
					<li id="experts-bot2"></li>
					<li id="experts-bot3"></li>
				</ul>
			</div>

		</div>

	</div>
</div>
