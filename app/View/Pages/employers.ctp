<div class="content-wrapp turquoise-banner-degree">
	<div class="wrapp">

		<div class="banner-employers">
			<div class="banner-head-left">
				<img src="/img/img-employers.png" />
			</div>
			<div class="banner-employers-right-small">
				<p>
					Much more than just<br/>
					a second opinion…<br/>
					<span class="dark-blue">advice and advocacy<br/>when you need it most.</span>
				</p>
			</div>
		</div>

	</div>
</div>

<div class="content-wrapp header-bg">
	<div class="wrapp">
		<div class="submenu-wrapp"></div>
	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">

		<div id="employers-infobox">

			<div class="experts-banner-subtitle dark-blue">
				Advance Medical is the global leader in Expert Medical Opinions.
			</div>

			<div class="employers-banner-subtitle turq">
				We have been helping patients to make better decisions since 1999.
			</div>

		</div>

		<div class="expert-info-title dark-blue">
			INFORMATION FOR EMPLOYERS
		</div>
		
		<div class="employers-info-subtitle turq">
			Expert Medical Opinion
		</div>

	
		
		<div class="expert-info-title-txt">
			<div class="expert-info-title-txt-left dark-grey">
				<p>
					The Expert Medical Opinion offers your employees the unique opportunity to have their medical situation analyzed by thought-leaders in their area of medical need, regardless of where they live. Getting the input from the world’s best medical minds ensures that all of the options for care have been vetted, discussed and described to your members. Throughout the process, patients and their caregivers have access to an unparalleled level of personal support through a dedicated Physician Case Manager within Advance Medical.
				</p>
			</div>
			
			<div class="employers-btn" onClick="window.location='/contact-us'">
					<div class="employers-btn-txt">
						CONTACT US NOW
					</div>
				</div>
		</div>

		<div id="what-included">
			<div id="what-included-title" class="turq">
				What is included
			</div>

			<div class="what-box">
				<div class="what-box-num turq">
					
				</div>
				<div class="what-box-txt dark-blue">
					<span class="turq">Direct and unlimited<br/>
					access to a</span><br/>
					Physician Case Manager
				</div>
			</div>

			<div class="what-box">
				<div class="what-box-num turq">
					
				</div>
				<div class="what-box-txt dark-blue">
					<span class="turq">Collection of</span><br/>
					medical records <span class="turq">on<br/>
					behalf of the patient</span>
				</div>
			</div>

			<div class="what-box">
				<div class="what-box-num turq">
					
				</div>
				<div class="what-box-txt dark-blue">
					<span class="turq">Review of</span><br/>
					medical records<br/>
					<span class="turq">by a physician</span>
				</div>
			</div>

			<div class="what-box">
				<div class="what-box-num turq">
					
				</div>
				<div class="what-box-txt dark-blue">
					Peer level support<br/>
					<span class="turq">to treating physician</span>
				</div>
			</div>

			<div class="what-box">
				<div class="what-box-num turq">
					
				</div>
				<div class="what-box-txt dark-blue">
					<span class="turq">Resource for</span><br/>
					finding and vetting<br/>
					<span class="turq">new providers</span>
				</div>
			</div>

			<div class="what-box">
				<div class="what-box-num turq">
					
				</div>
				<div class="what-box-txt dark-blue">
					<span class="turq">Opinions on</span><br/>
					diagnosis and treatment<br/>
					plans <span class="turq">from leading<br/>
					expert specialists</span>
				</div>
			</div>
		</div>

		<div class="what-sep"></div>

		<div class="employers-info-title dark-blue">
			WHY OFFER EXPERT MEDICAL OPINION<br/>TO YOUR EMPLOYEES?
		</div>

		<div class="diff-box-left">
			<div class="diff-box-title">
				PROVEN OUTCOMES.<br/>
				YOUR EMPLOYEES GET<br/>
				BETTER CARE. FOR LESS.
			</div>
			<div class="diff-box-sep"></div>
			<div class="diff-box-txt">
				Our program is based on actual clinical data from medical records, not claims or self reports. The Expert Medical Opinion service has proven itself to have a measurable positive impact on both health outcomes and benefits spending.
			</div>
		</div>

		<div class="diff-box-right">
			<div class="diff-box-title">
				YOUR EMPLOYEES GET <br/>
				CONCIERGE-STYLE SUPPORT <br/>
				FROM START TO FINISH
			</div>
			<div class="diff-box-sep"></div>
			<div class="diff-box-txt">
				Believe it or not, we really take care of all the details. We collect the medical records, we find new providers within the network, and most importantly, the Physician Case Manager is available to discuss any point of concern. They spend as much time as needed for each member and are actively involved as an informed, independent advocate making sure that the member is getting the best care possible.
			</div>
		</div>

		<div class="diff-box-left">
			<div class="diff-box-title">
				EXPERTS ARE SPECIFICALLY<br/>
				CHOSEN FOR EACH PATIENT
			</div>
			<div class="diff-box-sep"></div>
			<div class="diff-box-txt">
				Every patient has a unique set of circumstances. Depending on the exact diagnosis and condition, we do a global search to find the most relevant successful and experienced physicians in the world, without limiting ourselves to any particular hospital networks or predefined physician lists.
			</div>
		</div>

		<div class="diff-box-right">
			<div class="diff-box-title">
				UNLIMITED ACCESS TO<br/>
				A PHYSICIAN CASE MANAGER
			</div>
			<div class="diff-box-sep"></div>
			<div class="diff-box-txt">
				Our Physician Case Managers spend as much time as needed with each patient, actively involving them in the decision-making process. In addition, they are available to share findings with the patient’s primary care providers.
			</div>
		</div>


		<div id="provide">
			<div id="provide-included-title" class="turq">
				We also provide:
			</div>

			<div class="what-box">
				<div class="provide-box-num turq">
					<img src="/img/provide-icon1.jpg" />
				</div>
				<div class="what-box-txt dark-blue">
					Integration with<br/>
					<span class="turq">employee services<br/></span>
				</div>
			</div>

			<div class="what-box">
				<div class="provide-box-num turq">
					<img src="/img/provide-icon2.jpg" />
				</div>
				<div class="what-box-txt dark-blue">
					Claims impact, <br/>
					medical impact,<br/>
					<span class="turq">and participant satisfaction</span>
				</div>
			</div>

			<div class="what-box">
				<div class="provide-box-num turq">
					<img src="/img/provide-icon3.jpg" />
				</div>
				<div class="what-box-txt dark-blue">
					Engagement<br/>
					resources <span class="turq">to<br/>
					increase utilization</span>
				</div>
			</div>
		</div>


	</div>
</div>

<div class="content-wrapp turquoise-banner-degree">
	<div class="wrapp">

		<div id="experts-banner-box" style="height: 280px;">

			<div id="provide-banner-img">
				<img src="/img/krames-elliot.jpg" />
			</div>

			<div class="provide-slider-wrapp">
				<div class="opinion-slider">
					<div class="experts-slider-txt">
						"In a busy world, opinions of physicians are often made in haste and often made according to the physician's knowledge base and experience. That base of experience and knowledge might be great or might be less than good.”
					</div>
					<div class="experts-slider-subtxt dark-blue">
						<strong>Dr. Elliot S. Krames</strong><br/>
						Board Member Emeritus, International Neuromodulation Society (INS);<br/>
						Editor in Chief, Emeritus, Neuromodulation:  Journal of the INS<br/>
						San Francisco, CA
					</div>
				</div>
			</div>

		</div>

	</div>
</div>
<!--
<div class="content-wrapp">
	<div class="wrapp">

		<div class="employers-info-title2 dark-blue">
			DID YOU KNOW?
		</div>

		<div class="know-box">
			<div class="know-box-title dark-blue">
				<span class="turq">“The Institute of Medicine estimates that 30% of health care spending ($750
				billion) is spent on</span> unnecessary or poorly delivered health services.”
			</div>
			<div class="know-box-sep"></div>
			<div class="know-box-txt dark-grey">
				NEW YORK TIMES
			</div>
			<a href="http://www.nytimes.com/2012/09/11/opinion/waste-in-the-health-care-system.html?_r=2&" target="_blank">
				<div class="know-btn">
					<div class="know-btn-txt">
						READ MORE
					</div>
				</div>
			</a>

		</div>

		<div class="know-box">
			<div class="know-box-title dark-blue">
				<span class="turq">“$5 billion could be saved annually if<br/>
				patients avoided</span> the 5 most common<br/>
				unnecessary procedures <span class="turq">in each<br/>
				discipline.”</span>

			</div>
			<div class="know-box-sep"></div>
			<div class="know-box-txt dark-grey">
				CHOOSING WISELY
			</div>
			<a href="http://archinte.jamanetwork.com/article.aspx?articleid=1106007" target="_blank">
				<div class="know-btn">
					<div class="know-btn-txt">
						READ MORE
					</div>
				</div>
			</a>

		</div>

	</div>
</div>
-->

