<div class="content-wrapp blue-degree-bg">
	<div class="wrapp">

		<div class="banner-head">
			<div class="banner-head-left">
				<img src="/img/img-why-advance.png" />
			</div>
			<div class="banner-head-right-small">
				<p>
					With complex cases & diseases,<br/>
					the importance of having another opinion<br/>
					<span class="blue">...is that it can change your life</span>
				</p>
			</div>
		</div>

	</div>
</div>

<div class="content-wrapp header-bg">
	<div class="wrapp">
		<div class="submenu-wrapp"></div>
	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">

		<div class="aboutus-info-title dark-blue">
			WHY ADVANCE MEDICAL
		</div>
		<div class="aboutus-info-txt blue-sea">
			It’s simple. Advance Medical is the global leader in Expert Medical Opinions.
		</div>
		<div id="why-box">
			<div id="why-box-txt" class="dark-grey">
				We have been helping patients to make better decisions for almost 15 years, always with quality as our number one priority. With offices in 5 countries and a team of over 250 Physician Case Managers who together speak over 20 languages, you can rest assured we can help you find the answers you need.
			</div>
			<div class="contact-box-content cb1" style="margin-left:102px;">
				<div class="circle-icon">
					<img src="/img/icon-circle1.jpg" />
				</div>
				<div class="circle-wrapp">
					<div class="circle1">
						<div class="chart ch1 percent blue-sea" data-percent="98">98</div>
						<div class="contact-box circle"></div>
					</div>
				</div>
				<div class="percent-txt blue-sea"> 
					Patient satisfaction
				</div>
				<div class="circle-shadow"></div>
			</div>
		</div>

		<div class="aboutus-info-title dark-blue">
			THE ADVANCE MEDICAL DIFFERENCE
		</div>
		<div class="difference-subtitle blue-sea">
			What Sets Us Apart
		</div>
		
		<div class="difference-box">
			<div class="difference-box-number blue-sea">
				1
			</div>
			<div class="difference-box-title blue-sea">
				DOCTORS GUIDE YOU<br/>
				THROUGH YOUR CASE
			</div>
			<div class="difference-box-txt dark-grey">
				From the day your case begins to the day it ends, you will have access to an unparalleled level of personal support through your dedicated Physician Case Manager, who will help to guide you through the process and ensure you have all the clarity you need.
			</div>
		</div>

		<div class="difference-box2">
			<div class="difference-box-number blue-sea">
				2
			</div>
			<div class="difference-box-title blue-sea">
				SUPPORT THAT<br/>
				WILL ASTOUND YOU
			</div>
			<div class="difference-box-txt dark-grey">
				Believe it or not, we really take care of all the details. We collect your medical records for you, coordinating with as many of your previous doctors as necessary in order to get the relevant medical information, allowing you to focus on what really matters.
			</div>
		</div>

		<div class="difference-box">
			<div class="difference-box-number blue-sea">
				3
			</div>
			<div class="difference-box-title blue-sea">
				OUR CUSTOMIZED SEARCH<br/>
				FOR GLOBAL EXPERTS
			</div>
			<div class="difference-box-txt dark-grey">
				Every patient has a unique set of circumstances.
				Depending on your exact diagnosis and condition, we do a global search to find the most relevant, successful and experienced physicians in the world.
			</div>
		</div>

		<div class="difference-box2">
			<div class="difference-box-number blue-sea">
				4
			</div>
			<div class="difference-box-title blue-sea">
				PATIENTS GET THE CRITICAL<br/>
				INFORMATION THEY NEED
			</div>
			<div class="difference-box-txt dark-grey">
				When patients get an Expert Medical Opinion, experts make a major change to the treatment plan 21% of the time and make a major change to the patient’s original diagnosis 7% of the time. Everyone gets the incredible peace of mind that comes from knowing what the world's leading experts recommend.
			</div>
		</div>

		<div class="second-subtitle blue-sea">
			<span class="dark-blue">Much More Than Just a Second Opinion...</span>
		</div>

		<div class="second-box">
			<div class="second-box-picto">
				<img src="/img/second-picto1.jpg" />
			</div>
			<div class="second-box-txt">
				We provide you with the help you
				need most. We assist with the
				collection of your medical records, serve as a resource for finding and
				vetting new providers and often request the opinion of more than one leading global expert. 

			</div>
		</div>

		<div class="second-box-middle">
			<div class="second-box-picto">
				<img src="/img/second-picto2.jpg" />
			</div>
			<div class="second-box-txt">
				Throughout the process, our Physician Case Managers are available for ongoing dialogue about your case and medical condition. These highly-trained physicians have the time and expertise to be both your advocate and advisor.
			</div>
		</div>

		<div class="second-box-right">
			<div class="second-box-picto">
				<img src="/img/second-picto3.jpg" />
			</div>
			<div class="second-box-txt">
				Your Expert Medical Opinion report includes a consolidated summary of your medical history, experts’ written recommendations, experts’ CVs and answers to your specific questions.
			</div>
		</div>

	</div>
</div>