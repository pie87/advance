<div class="content-wrapp blue-degree-bg">
	<div class="wrapp">

		<div class="banner-terms"></div>

	</div>
</div>

<div class="content-wrapp header-bg">
	<div class="wrapp">
		<div class="submenu-wrapp"></div>
	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">

		<div id="terms-infobox">

			<div class="expert-info-title dark-blue">
				PRIVACY NOTICE
			</div>

			<div class="terms-banner-subtitle blue-sea">
				This notice describes how medical information about you may be<br/>used and disclosed, and how you can get access to this information.<br/>Please review it carefully.
			</div>

			<div id="terms-info">
				<p>This Notice describes the privacy practices of Advance Medical, Inc. which is referred to as &ldquo;Advance Medical&rdquo; in this Notice.&nbsp; This Notice applies to uses and disclosures of medical information collected by Advance Medical about persons in the Expert Medical Opinion program.&nbsp; We are required by law to comply with this Notice.&nbsp; Other policies may apply with respect to information collected from persons residing in other countries.</p>
				<p>&nbsp;</p>
				<p>To receive another copy of this notice, electronically or on paper, call 617-9870018, send an e-mail to <a href="mailto:privacyofficer@advance-medical.com">privacyofficer@advance-medical.com</a> or send a written request to:&nbsp; Advance Medical, Privacy Officer, 100 Lowder Brook Drive, Suite 1400, Westwood, MA 02090.&nbsp;&nbsp;</p>
				<p>&nbsp;</p>
				<p>Advance Medical may change this policy at any time and without prior notice.&nbsp; Those changes will apply to any protected health information already held by Advance Medical.&nbsp; Advance Medical will keep the current policy on the website and available at its offices.&nbsp; &nbsp;</p>
				<p>&nbsp;</p>
				<p><strong>Uses and Disclosures</strong></p>
				<p>Advance Medical may collect protected health information for use in our Expert Medical Opinion service (the &ldquo;Service&rdquo;).&nbsp; The information will be used and disclosed in creating a medical case and history, identifying physician consultants, and producing the Expert Medical Opinion Report (the &ldquo;Report&rdquo;).&nbsp; &nbsp;</p>
				<p>&nbsp;</p>
				<p>Advance Medical usually will remove some personal identifiers (such as name and address) before disclosing your information to expert physicians, but certain personal details may be disclosed and later reflected in the Report.&nbsp;&nbsp;&nbsp;&nbsp;</p>
				<p>&nbsp;</p>
				<p>Advance Medical may disclose your health information to case managers, clinical committee members, administrators who will use the information to process your case and other individuals who are involved in providing the Service or generating your Report.&nbsp; In some cases your information may be sent to an outside consulting physician or other consulting medical professionals.&nbsp; For&nbsp; example, a case manager may share your information with a medical director in order to identify an appropriate consulting physician for your case.&nbsp; The case manager may share the information with the consulting physician.&nbsp; We will ask these consultants to sign agreements requiring them to preserve the confidentiality of this information.</p>
				<p>&nbsp;</p>
				<p><strong>Payment</strong></p>
				<p>We do not use your health information for payment purposes.</p>
				<p>&nbsp;</p>
				<p><strong>Operations</strong></p>
				<p>Advance Medical also may use your health information to review or evaluate the performance of our systems in providing the Service to you, to improve the quality or timeliness of our services.&nbsp; &nbsp;</p>
				<p>&nbsp;</p>
				<p>Advance Medical also may create de-identified information based upon information you have provided to us.&nbsp; De-identified information is information that does not include your name, address, birth date, or other information that could be used to identify you.&nbsp; This de-identified information could be used for quality improvement, research and other purposes.&nbsp; For example, ADVANCE</p>
				<p>MEDICAL could use this de-identified information to demonstrate the reliability of our information management systems or to generate medical research information. &nbsp;We would not identify you by name in any resulting reports or other information.</p>
				<p>&nbsp;</p>
				<p>Advance Medical may disclose information in order to contact you during the course of providing services to you as either part of the ongoing process or as part of an effort to follow-up with you after using the Service or if there was an opportunity to inform you about additional services of interest.&nbsp; We may contact you through the mail, over e-mail or through the phone.</p>
				<p>&nbsp;</p>
				<p>Advance Medical may disclose protected heath information for the following purposes without your authorization:</p>
				<p>&nbsp;&sect; As required by law</p>
				<ul>
				<li>For public health activities</li>
				<li>To protect victims of abuse, neglect or domestic violence</li>
				<li>For health oversight activities carried out by government agencies</li>
				<li>For judicial and administrative proceedings after proper legal process</li>
				<li>For law enforcement purposes</li>
				</ul>
				<p>Other uses or disclosures about your medical information may require your written authorization.&nbsp; The patient can revoke that authorization at any time but that revocation will not affect any use or disclosure made prior to revocation.</p>
				<p>&nbsp;</p>
				<p>Advance Medical may disclose information to you, to your representative or to another individual designated by you.&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p><strong>Individual&rsquo;s Rights</strong></p>
				<p>You have the right to inspect, copy and amend completed medical records maintained by Advance Medical.&nbsp; To inspect your medical records please make a signed and dated written request to Advance Medical, Privacy Officer, 100 Lowder Brook Drive, Suite 1400, Westwood, MA 02090.&nbsp; We may charge you a processing fee for these requests.&nbsp; In some cases we may not honor your requests, such as if disclosing records will cause you harm or if they are part of legal proceedings or if they are part of ongoing research.&nbsp; In the event that we deny your request you will be notified of any denial within 60 days and be given additional options or information.&nbsp; To make an amendment to your medical records, please make a signed and dated written request to Advance Medical, Privacy Officer, 100 Lowder Brook Drive, Suite 1400, Westwood, MA 02090.&nbsp; In the request please describe the changes that you would like to make and the reason why.&nbsp; In the event that we deny your request you will be notified of any denial within 60 days and be given additional options or information.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
				<p>&nbsp;</p>
				<p>You have the right to request restricted disclosures or uses or to request that we limit access to your personal health information.&nbsp; Please make a signed and dated written request to Advance Medical, Privacy Officer, 100 Lowder Brook Drive, Suite 1400, Westwood, MA 02090.&nbsp; Please include all of the specific information that you want restricted and the person or categories of&nbsp; persons who should or should not have access to the information.&nbsp; We have the right to deny your requests or ask for additional information.&nbsp; In the event that we deny your request you will be notified of any denial within 60 days.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
				<p>&nbsp;</p>
				<p>You have the right to an accounting of certain disclosures of your personal health information that were made without your written authorization.&nbsp; Please make a signed and dated written request to Advance Medical, Privacy Officer, 100 Lowder Brooke Drive, Suite 1400, Westwood, MA 02090.&nbsp; We are only obligated to share disclosure accounting for the preceding six years.&nbsp; This accounting will not include disclosures made in the course of providing the Service or generating the Report.</p>
				<p>&nbsp;</p>
				<p>You have the right to request to receive confidential communications about your health information, such as having information sent to a particular address or in a particular way.&nbsp; Please make a signed and dated written request to Advance Medical, Privacy Officer, 100 Lowder Brook Drive, Suite 1400, Westwood, MA 02090. In your request specify how you would like us to communicate with&nbsp; you.&nbsp;</p>
				<p>&nbsp;</p>
				<p>You have the right to make complaints about any possible violation of your Privacy Rights to Advance Medical.&nbsp; Advance Medical will not penalize you for making a complaint.&nbsp; To make a complaint to Advance Medical, please make a signed and dated written complaint to Advance Medical, Privacy Officer, 100 Lowder Brook Drive, Suite 1400, Westwood, MA 02090.&nbsp;</p>
				<p>&nbsp;</p>
				<p>If, for any reason, you would like to discuss any matter concerning our privacy policies or to request copies of our privacy policies, please contact us at:</p>
				<p>&nbsp;</p>
				<p>Privacy Officer</p>
				<p>c/o Advance Medical</p>
				<p>100 Lowder Brook Drive, Suite 1400</p>
				<p>Westwood, MA 02090</p>
				<p>617-987-0018</p>
				<p>privacyofficer@advance-medical.com</p>
				<p>&nbsp;</p>
				<p>Effective as of January 31, 2014.</p>
				<p>Rev 1 Advance Medical, Inc 2014 &copy;</p>
				<p>&nbsp;</p>
			</div>

		</div>
	</div>
</div>

