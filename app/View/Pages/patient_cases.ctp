<div class="content-wrapp blue-degree-bg">
	<div class="wrapp">

		<div class="banner-head">
			<div class="banner-head-left">
				<img src="/img/img-patient-cases.png" />
			</div>
			<div class="banner-head-right-small">
				<p>
					Every year thousands of patients<br/>
					get better care after connecting to<br/>
					Advance Medical. 
				</p>
			</div>
		</div>

	</div>
</div>

<div class="content-wrapp header-bg">
	<div class="wrapp">
		<div class="submenu-wrapp"></div>
	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">

		<div id="patient-banner-subtitle-box">

			<div class="patient-banner-subtitle blue-sea">
				All our patients have one thing in common - the desire to make <br/>
				better decisions about their health.
			</div>

			<div class="patient-banner-subtitle2 dark-grey">
				From life-threatening diseases and surgeries to migraines and insomnia,<br/>
				we can help you get the information you need to get your life back in order.
			</div>

		</div>

		<div class="aboutus-info-title dark-blue">
			PATIENT STORIES
		</div>

	</div>
</div>

<!-- PATIENT BOX -->

<div class="content-wrapp">
	<div class="wrapp">
		
		<div class="patient-name-box" style="margin: 70px 363px 45px 364px;">
			<div class="patient-sep"></div>
			<div class="patient-name blue-sea">
				Shannon
			</div>
			<div class="patient-sep"></div>
		</div>

		<div class="patient-box-txt-left">
			<div class="patient-box-txt-title blue-sea">
				WHY SHANNON CAME TO US:
			</div>
			<div class="patient-box-txt-txt dark-grey">
				<p>Shannon, an investment banker in her 40s, went to the doctor after having recurring pain in her right armpit.  Her doctor thought it might be a tumor and performed a skin biopsy. Shannon was diagnosed with infiltrating basal cell carcinoma.  Although basal cell carcinoma is the most commonly occurring skin cancer, it should never be taken lightly. Shannon and her treating doctor made the right call to get a second opinion.  She then went to a second oncologist, who had the tissue sample reanalysed. This second oncologist gave Shannon a different diagnosis (differentiated cell carcinoma) and recommended chemotherapy. One week after beginning chemotherapy, Shannon learned that she had Expert Medical Opinion coverage through her employer.  She came to Advance Medical to ensure that her second diagnosis was in fact correct. </p>
			</div>
		</div>

		<div class="patient-box-txt-right">
			<div class="patient-box-txt-title blue-sea">
				OUTCOMES:
			</div>
			<div class="patient-box-txt-txt dark-grey">
				<p>Advance Medical sent Shannon’s tissue samples to a globally renowned Boston-based dermatopathologist. He diagnosed Shannon with metastatic porocarcinoma, a very rare form of cancer that is often misdiagnosed.  Because of its rarity, morphologic peculiarity and ambiguous similarity to other carcinomas, guidelines and strong recommendations are not widely available and, thus, can be missed. The case was then reviewed by two clinical oncologists recognized worldwide as key leaders in this type of skin cancer – one from Berlin and one from New York. Both specialists independently evaluated her case and arrived at the same conclusion. Given the new diagnosis, the clinical experts recommended radical changes to Shannon’s treatment plan - they advised her to stop chemotherapy and immediately have an operation to remove her tumor.  Advance Medical identified a surgeon that was highly-qualified, was in her insurance plan’s network, and could see her the following week.  Shannon followed the experts’ treatment proposal and had a smooth recovery.</p><br/>
			

			</div>
		</div>


	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">
		<div class="patient-sep-line"></div>
	</div>
</div>

<!--
<div class="content-wrapp"><div class="wrapp"><div class="pico"></div></div></div>
<div class="content-wrapp blue-title-bg">
	<div class="wrapp">

		<div class="patient-banner-title2">
			“After her case was complete, Shannon told me, ‘I don’t want to imagine what would have happened if I hadn’t decided to get an Expert Medical Opinion’.”
		</div>
		<div class="patient-banner-name">
			<span class="dark-blue"><strong>DR. HEATHER TOWERY</strong></span><br/>
			Shannon’s Physician<br/>Case Manager
		</div>

	</div>
</div>
-->

<!-- END PATIENT BOX -->

<!-- PATIENT BOX -->

<div class="content-wrapp">
	<div class="wrapp">
		
		<div class="patient-name-box" style="margin: 70px 368px 45px 386px;">
			<div class="patient-sep"></div>
			<div class="patient-name blue-sea">
				Emily
			</div>
			<div class="patient-sep"></div>
		</div>

		<div class="patient-box-txt-left">
			<div class="patient-box-txt-title blue-sea">
				WHY EMILY CAME TO US:
			</div>
			<div class="patient-box-txt-txt dark-grey">
				<p>Emily, an accountant in her 30s, had suffered from back pain for over two years when her pain began to worsen. She was seen by a chiropractor, a pain clinic, a psychologist, and finally an orthopedic surgeon, who recommended a spinal fusion.  Emily felt hesitant about the recommendation, especially given that she would have to be away from work for a long recovery.  She came to Advance Medical confused, wanting to better understand the cause of her pain and to know if surgery was her only alternative. </p>
			</div>
		</div>

		<div class="patient-box-txt-right">
			<div class="patient-box-txt-title blue-sea">
				OUTCOMES:
			</div>
			<div class="patient-box-txt-txt dark-grey">
				<p>Advance Medical collected Emily’s imaging, tests and notes from all of her doctors and sent her case to two leading experts – a Houston-based physiatrist (a specialist in non-surgical pain management) and a New York City-based neurosurgeon. Both experts arrived at the same conclusion – Emily’s condition did not warrant surgery as it was highly unlikely to substantially alter her level of pain and even had a significant chance of worsening it. The experts carefully outlined two new pain-reduction strategies that promised to have a high probability of success: core strengthening and a trial of a nerve blocking therapy. These two methods relieved her pain to a manageable level and Emily was able to return to her regular active life.</p><br/> 

			</div>
		</div>


	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">
		<div class="patient-sep-line"></div>
	</div>
</div>

<!--
<div class="content-wrapp"><div class="wrapp"><div class="pico"></div></div></div>
<div class="content-wrapp blue-title-bg">
	<div class="wrapp">

		<div class="patient-banner-title">
			“Emily’s case shows just how important it is to ensure that all options are considered before going ahead with surgery.”
		</div>
		<div class="patient-banner-name">
			<span class="dark-blue"><strong>DR. BARB WALSH</strong></span><br/>
			Emily’s Physician<br/>Case Manager
		</div>

	</div>
</div>
-->

<!-- END PATIENT BOX -->

<!-- PATIENT BOX -->

<div class="content-wrapp">
	<div class="wrapp">
		
		<div class="patient-name-box" style="margin: 70px 361px 45px 376px;">
			<div class="patient-sep"></div>
			<div class="patient-name blue-sea">
				Andrea
			</div>
			<div class="patient-sep"></div>
		</div>

		<div class="patient-box-txt-left">
			<div class="patient-box-txt-title blue-sea">
				WHY ANDREA CAME TO US:
			</div>
			<div class="patient-box-txt-txt dark-grey">
				<p>Andrea was a teenager with a “sensitive stomach” which frequently manifested in stomach aches, diarrhea and occasionally vomiting.  Her medical journey included extensive trips to her pediatrician and different specialists.  Andrea’s doctor did an intestinal biopsy and diagnosed her with Crohn’s disease – an autoimmune inflammatory bowel disease.  Andrea was given steroid treatment which made her face swell and her parent’s noticed that she was very moody. Andrea’s parents came to Advance Medical wanting to ensure that Andrea was getting the right care. They hoped to find a medication with milder side effects and do everything possible to create an appropriate diet for Andrea.</p>
			</div>
		</div>

		<div class="patient-box-txt-right">
			<div class="patient-box-txt-title blue-sea">
				OUTCOMES:
			</div>
			<div class="patient-box-txt-txt dark-grey">
				<p>Advance Medical collected Andrea’s records and test results and sent her case to two experts in inflammatory bowel disease – both at world-renowned pediatric medical centers. Andrea’s parents were shocked to learn that both experts found no evidence to support the diagnosis of Crohn’s disease. The experts recommended that Andrea be taken off the steroid medication. The experts did, however, find sufficient evidence to diagnose Andrea with Irritable Bowel Syndrome. They recommended a specific dietary plan and suggested that the family keep a journal of Andrea’s meals to facilitate further discussion with Andrea’s current doctor. The family, pleased to take Andrea off the steroid medication, put Andrea on the new diet and began to see near immediate improvements in Andrea’s condition.</p>
			</div>
		</div>


	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">
		<div class="patient-sep-line"></div>
	</div>
</div>

<!--
<div class="content-wrapp"><div class="wrapp"><div class="pico"></div></div></div>
<div class="content-wrapp blue-title-bg">
	<div class="wrapp">

		<div class="patient-banner-title2">
			“After months of doctor’s appointments for the same issue, Andrea’s mother was delighted to finally have an accurate diagnosis and to take Andrea off the steroid medication.”
		</div>
		<div class="patient-banner-name">
			<span class="dark-blue"><strong>DR. NICHOLAS SWANE</strong></span><br/>
			Andrea’s Physician<br/>Case Manager
		</div>

	</div>
</div>
-->

<!-- END PATIENT BOX -->

<!-- PATIENT BOX -->

<div class="content-wrapp">
	<div class="wrapp">
		
		<div class="patient-name-box" style="margin: 70px 363px 45px 399px;">
			<div class="patient-sep"></div>
			<div class="patient-name blue-sea">
				Sue
			</div>
			<div class="patient-sep"></div>
		</div>

		<div class="patient-box-txt-left">
			<div class="patient-box-txt-title blue-sea">
				WHY SUE CAME TO US:
			</div>
			<div class="patient-box-txt-txt dark-grey">
				<p>Sue, a 35 year old engineer, began experiencing episodes where she felt her heart racing or heart palpitations. When an echocardiogram revealed that she had aortic valve stenosis, a narrowing in the valve that carries blood from her heart to the rest of her circulatory system, her cardiologist told her that surgery was not necessary to repair it at this time, but that she could take medication.  She then approached a second cardiologist for a second opinion who recommended heart surgery in the near-future to repair the valve. When Sue came to Advance Medical she was in a state of panic – completely confused and extremely scared about having a potentially life-threatening disease and needing heart surgery. Both cardiologists she had seen were very experienced, at large academic medical centers, and seemed to be completely convinced regarding their own opinions. </p>
			</div>
		</div>

		<div class="patient-box-txt-right">
			<div class="patient-box-txt-title blue-sea">
				OUTCOMES:
			</div>
			<div class="patient-box-txt-txt dark-grey">
				<p>Advance Medical collected Sue’s records and sent her case to two renowned Los Angeles-based cardiologists and a Chicago-based heart surgeon. All three experts agreed that the palpitations, although scary to experience, were not indicating that she had a life-threatening disease.  They thought that surgery was unnecessary and confirmed that her level of aortic valve stenosis would have no short or long term effects on her quality of life. Sue was incredibly relieved to learn that the experts shared the same opinion and thrilled to put the idea of heart surgery behind her.</p>
			</div>
		</div>


	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">
		<div class="patient-sep-line"></div>
	</div>
</div>

<!--
<div class="content-wrapp"><div class="wrapp"><div class="pico"></div></div></div>
<div class="content-wrapp blue-title-bg">
	<div class="wrapp">

		<div class="patient-banner-title2">
			“All Sue kept saying to me was, ‘I can’t believe you got these incredible experts to look at my case!’ She was so pleased to get peace of mind.”
		</div>
		<div class="patient-banner-name">
			<span class="dark-blue"><strong>DR. CRAIG SYLVESTER </strong></span><br/>
			Sue’s Physician<br/>Case Manager
		</div>

	</div>
</div>

-->

<!-- END PATIENT BOX -->

<!-- PATIENT BOX -->

<div class="content-wrapp">
	<div class="wrapp">
		
		<div class="patient-name-box" style="margin: 70px 369px 45px 391px;">
			<div class="patient-sep"></div>
			<div class="patient-name blue-sea">
				Mike
			</div>
			<div class="patient-sep"></div>
		</div>

		<div class="patient-box-txt-left">
			<div class="patient-box-txt-title blue-sea">
				WHY MIKE CAME TO US:
			</div>
			<div class="patient-box-txt-txt dark-grey">
				<p>Mike, a 50 year old energy executive, went to see two neurologists after having problems with numbness and double vision. He was diagnosed with multiple sclerosis and prescribed a very expensive medication (interferon) that was expected to cause him serious side effects. He immediately became worried about how this would impact him, both personally and professionally. Mike hated the idea of living with the drug’s side effects and came to Advance Medical wanting to explore alternative treatments.</p>
			</div>
		</div>

		<div class="patient-box-txt-right">
			<div class="patient-box-txt-title blue-sea">
				OUTCOMES:
			</div>
			<div class="patient-box-txt-txt dark-grey">
				<p>Advance Medical collected Mike’s records and sent them to multiple sclerosis experts at leading reference centers in Houston and London. Both confirmed that Mike did not actually have multiple sclerosis and that the expensive medication that he had been prescribed would not improve his condition. They recommended additional tests that would provide the information needed for a correct diagnosis. Mike, very surprised to learn that he did not actually have multiple sclerosis, took his reports back to his treating doctors who learned, after the additional recommended tests were performed, that Mike actually had acephalic migraines (“silent migraines”).  With the right diagnosis, Mike could get the treatment that was right for him.</p>
			</div>
		</div>


	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">
		<div class="patient-sep-line"></div>
	</div>
</div>

<!--
<div class="content-wrapp"><div class="wrapp"><div class="pico"></div></div></div>
<div class="content-wrapp blue-title-bg">
	<div class="wrapp">

		<div class="patient-banner-title">
			“Imagine his shock – Mike found out he didn’t actually have multiple sclerosis.”
		</div>
		<div class="patient-banner-name">
			<span class="dark-blue"><strong>DR. ETHAN ELLIS </strong></span><br/>
			Mike’s Physician<br/>Case Manager
		</div>

	</div>
</div>
-->

<!-- END PATIENT BOX -->

<!-- PATIENT BOX -->

<div class="content-wrapp">
	<div class="wrapp">
		
		<div class="patient-name-box" style="margin: 70px 369px 45px 391px;">
			<div class="patient-sep"></div>
			<div class="patient-name blue-sea">
				Toby
			</div>
			<div class="patient-sep"></div>
		</div>

		<div class="patient-box-txt-left">
			<div class="patient-box-txt-title blue-sea">
				WHY TOBY CAME TO US:
			</div>
			<div class="patient-box-txt-txt dark-grey">
				<p>Seven year old Toby had always been a picky eater with a preference for soft foods. His parents were only mildly concerned until one night when a solid bite of food during dinner set Toby into a state of panic. After this incident, he began to avoid solid foods altogether, losing 15 pounds in several months. His parents, now extremely concerned, took Toby to a variety of specialists. Toby’s doctors suggested that the problem arose from swallowing pain and recommended throat surgery as a way to assist with the symptoms. His parents felt uncomfortable with the recommendation and wanted to know if there were any non-surgical treatments that could help Toby.</p>
			</div>
		</div>

		<div class="patient-box-txt-right">
			<div class="patient-box-txt-title blue-sea">
				OUTCOMES:
			</div>
			<div class="patient-box-txt-txt dark-grey">
				<p>Advance Medical took Toby’s case to a leading pediatrician who specialized in gastroenterology and nutrition and a well-known surgeon from a top center of excellence for gastroenterological conditions. The experts agreed that the surgery was not necessary. They believed Toby showed signs of choking phobia and recommended that he see a pediatric psychologist. Toby’s parents were extremely pleased to hear that surgery was not necessary and quickly followed the experts’ recommendations. Toby made a full recovery within 10 weeks. </p>
			</div>
		</div>


	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">
		<div class="patient-sep-line"></div>
	</div>
</div>

<!--
<div class="content-wrapp"><div class="wrapp"><div class="pico"></div></div></div>
<div class="content-wrapp blue-title-bg">
	<div class="wrapp">

		<div class="patient-banner-title2">
			“What a beautiful story – this poor little boy might have had unnecessary throat surgery if his parents had not been proactive enough to look elsewhere for answers.”
		</div>
		<div class="patient-banner-name">
			<span class="dark-blue"><strong>DR. KERRY MASSMAN </strong></span><br/>
			Toby’s Physician<br/>Case Manager
		</div>

	</div>
</div>
-->

<!-- END PATIENT BOX -->

<!-- PATIENT BOX -->

<div class="content-wrapp">
	<div class="wrapp">
		
		<div class="patient-name-box" style="margin: 70px 363px 45px 384px;">
			<div class="patient-sep"></div>
			<div class="patient-name blue-sea">
				Kevin
			</div>
			<div class="patient-sep"></div>
		</div>

		<div class="patient-box-txt-left">
			<div class="patient-box-txt-title blue-sea">
				WHY KEVIN CAME TO US:
			</div>
			<div class="patient-box-txt-txt dark-grey">
				<p>Two years before coming to Advance Medical, Kevin, a logistics manager in his 50s, visited the emergency room for severe abdominal pain. Imaging at the ER revealed an abnormal mass in his abdomen. Doctors discharged Kevin with some general advice, but did not give him instructions for next steps. As a result, Kevin followed their advice but didn’t schedule any follow-up evaluations of the mass.  Two years later he went to a gastroenterologist after a recurrence of the pain.   Tests showed the mass had grown significantly. Kevin’s doctor recommended the removal of his kidney and referred him to an urologist for evaluation. He came to Advance Medical wanting to know if there was any alternative to going forward with surgery.</p>
			</div>
		</div>

		<div class="patient-box-txt-right">
			<div class="patient-box-txt-title blue-sea">
				OUTCOMES:
			</div>
			<div class="patient-box-txt-txt dark-grey">
				<p>After explaining his concerns to his Physician Case Manager, Kevin’s case was sent to several leading urologists. These specialists included renowned urologists from academic medical centers in Tampa and Rochester. Both experts noted that it was inappropriate to go forward with surgery without first performing an RBC scan. Kevin took his report back to his treating physician and ordered the scan. The scan results confirmed that the mass near his kidney was actually a benign hemangioma, which meant that surgery was not necessary, and he could keep his kidney. </p>
			</div>
		</div>


	</div>
</div>

<!--
<div class="content-wrapp"><div class="wrapp"><div class="pico"></div></div></div>
<div class="content-wrapp blue-title-bg" style="margin-bottom:50px;">
	<div class="wrapp">

		<div class="patient-banner-title">
			“Kevin was incredibly happy to find out that additional tests were needed to confirm whether surgery was truly necessary.”
		</div>
		<div class="patient-banner-name">
			<span class="dark-blue"><strong>Dr. HEALTHER ALKLER </strong></span><br/>
			Kevin’s Physician<br/>Case Manager
		</div>

	</div>
</div>
-->

<!-- END PATIENT BOX -->