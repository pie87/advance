<div class="content-wrapp blue-degree-bg">
	<div class="wrapp">

		<div class="banner-head">

			<div class="banner-head-left-phy">
				<img src="/img/img-physician-case-manager.png" />
			</div>
			
			<div class="banner-head-right3">
				<p>
					“My Physician Case Manager was wonderful,<br/>
					giving me weekly email updates,<br/>
					explaining anything in the reports<br/>
					that was inscrutable medical jargon.<br/>
					I give her efforts on my behalf an A+.”<br/>
				</p>
				<div class="banner-head-name blue">
					<strong>Andrea</strong>, Niagara Falls
				</div>
			</div>
		</div>

	</div>
</div>

<div class="content-wrapp header-bg">
	<div class="wrapp">
		<div class="submenu-wrapp"></div>
	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">

		<div id="physician-infobox">

			<div class="expert-info-title dark-blue">
				THE PHYSICIAN CASE MANAGER’S ROLE
			</div>

			<div class="physician-banner-subtitle blue">
				The Expert Medical Opinion program pairs each patient with<br/>
				a designated Physician Case Manager - a doctor with years of clinical<br>
				 experience - to provide comprehensive assistance throughout the<br/>
				duration of your case.
			</div>

			<div class="expert-info-title-txt">
				<div class="expert-info-title-txt-left dark-grey">
					<p>
						You can think of your Physician Case Manager as your advisor and advocate – they will help you understand what is happening in your current care and regularly keep you updated about the status of your case. In addition to providing you with an unparalleled level of personal support, your Physician Case Manager will be the crucial link between you and the experts. They will work with you to ensure that the Expert’s insights that you receive are both understandable and actionable. Our Physician Case Managers speak over 20 languages  and bring a vast understanding of medicine and healthcare systems. They are clinically active in a multitude of specialities/sub-specialities including: internal medicine, pediatrics, oncology, cardiology, nephrology, occupational health, gynecology, emergency medicine, critical care/pulmonology, endocrinology and others.
					</p>
				</div>
			
			</div>

		</div>

		<div class="expert-info-title dark-blue">
			EXAMPLES OF OUR PHYSICIAN CASE MANAGERS
		</div>
		
		<div id="experts-people">
			<div id="expert1" class="def-hide" rel="1">
				<div class="expert-people-avatar">
					<img src="/img/experts/expert-phy-1.jpg" />
				</div>
				<div class="expert-people-avatar-name dark-grey">
					Barbara Walsh, M.D., FAAP
				</div>
			</div>

			<div id="expert2" class="def-hide" rel="2">
				<div class="expert-people-avatar">
					<img src="/img/experts/expert-phy-2.jpg" />
				</div>
				<div class="expert-people-avatar-name dark-grey">
					Ethan Ellis, M.D.
				</div>
			</div>

			<div id="expert3" class="def-hide" rel="3">
				<div class="expert-people-avatar">
					<img src="/img/experts/expert-phy-3.jpg" />
				</div>
				<div class="expert-people-avatar-name dark-grey">
					Craig Sylvester, M.D.
				</div>
			</div>

			<div id="expert4" class="def-hide" rel="4">
				<div class="expert-people-avatar">
					<img src="/img/experts/expert-phy-4.jpg" />
				</div>
				<div class="expert-people-avatar-name dark-grey">
					Kerry Massman, M.D.
				</div>
			</div>

			<div id="expert5" class="def-hide" rel="5">
				<div class="expert-people-avatar">
					<img src="/img/experts/expert-phy-5.jpg" />
				</div>
				<div class="expert-people-avatar-name dark-grey">
					Leonardo Riella, M.D., PhD
				</div>
			</div>

			<div id="expert6" class="def-hide" rel="6">
				<div class="expert-people-avatar">
					<img src="/img/experts/expert-phy-6.jpg" />
				</div>
				<div class="expert-people-avatar-name dark-grey">
					Heather Alker, M.D., MPH
				</div>
			</div>

			<div id="experts-people-box" class="blue-sea">
				<div class="expert-hand" style="margin: 70px 145px 10px 145px;"></div>
				<div class="experts-people-box-txt exp-txt-def" style="height:238px;">
					<?php if($this->request->is('mobile')){ echo "Tap on "; }else{ echo "Roll over ";} ?>images to<br/>
					learn more about our<br>
					Physician Case Managers
				</div>
				<div class="experts-people-box-txt exp-txt1 exp-txt-roll" style="height:228px;padding: 30px 20px 0px 20px;">
					<p>Pediatrics, Pediatric Emergency Medicine (Boarded)</p><br/>

					<p>Boston Children’s Hospital; American<br/>
					Academy of Pediatrics (Fellowship)</p><br/>

					<p>Boston Children’s Hospital
					<p>(Residency)</p><br/>

					University of Massachusetts Medical School (Medical School)
				</div>
				<div class="experts-people-box-txt exp-txt2 exp-txt-roll" style="height: 228px; padding: 30px 20px 0px; display: none;">
					<p>Internal Medicine, Cardiology<br/>
					(Boarded)</p><br/>

					<p>Beth Israel Deaconess Medical Center (Fellowship)</p><br/>

					<p>Beth Israel Deaconess Medical Center (Residency)</p><br/>

					<p>University of Rochester<br/>
					(Medical School)</p>
				</div>
				<div class="experts-people-box-txt exp-txt3 exp-txt-roll" style="height: 213px; padding: 45px 20px 0px; display: none;">
					<p>Emergency Medicine, Advanced Cardiac/Advanced Trauma Life Support (Boarded)</p><br/>

					<p>Baystate Medical Center - Chief Resident (Residency)</p><br/>

					<p>Tufts University School of Medicine<br/>
					(Medical School)</p>
				</div>
				<div class="experts-people-box-txt exp-txt4 exp-txt-roll" style="height:233px;padding: 25px 20px 0px 20px;">
					<p>Internal Medicine<br/>
					(Boarded)</p><br/>

					<p>Massachusetts General Hospital/<br/>
					Dana Farber (Fellowship)</p><br/>

					<p>Massachusetts General Hospital -<br/>
					Chief Resident (Residency)</p><br/>

					<p>University of Missouri-Columbia<br/>
					(Medical School)</p>
				</div>
				<div class="experts-people-box-txt exp-txt5 exp-txt-roll" style="height: 236px;padding: 22px 20px 0px 20px;">
					<p>Internal Medicine, Nephrology;<br/>
					Renal Transplant Certified (Boarded)</p><br/>

					<p>Brigham & Women’s / Massachusetts<br/>
					General Hospital, Harvard Medical School (Fellowship)</p><br/>

					<p>Brigham & Women’s, Harvard Medical School (Residency)</p><br/>

					<p>Federal University of Parana, Brazil<br/>
					(Medical School)</p><br/>
				</div>
				<div class="experts-people-box-txt exp-txt6 exp-txt-roll" style="height: 226px;padding: 30px 20px 0px 20px;">
					<p>Obstetrics & Gynecology<br/>
					(Boarded)</p><br/>

					<p>Beth Israel Deaconess Medical Center (Fellowship)</p><br/>

					<p>Tufts University; University of Massachusetts Medical School (Residency)</p><br/>

					<p>Tufts University Medical School<br/>
					(Medical School)</p><br/>
				</div>
			</div>
		</div>


	</div>
</div>

<div class="content-wrapp blue-title-bg">
	<div class="wrapp">

		<div id="experts-banner-box" style="height: 305px;">

			<div id="expert-banner-img">
				<img id="expert-img" src="/img/barbara-cita.png" />
			</div>

			<div class="opinion-slider-wrapp">
				<div id="opslide" class="opinion-slider">
					<div class="experts-slider-txt">
						“Since I can spend the time building a relationship with patients, they know that I am honest, independent, and that they can trust that I am advocating for them. For patients who are confused or concerned about their care, it’s invaluable to have our focused and unlimited attention.”
					</div>
					<div class="experts-slider-subtxt dark-blue">
						<strong>Barbara Walsh, M.D., FAAP</strong><br/>
						Pediatrics, Pediatric Emergency Medicine<br/>
						Boston Children’s Hospital; American Academy of Pediatrics<br/>
						University of Massachusetts Medical School

					</div>
				</div>
				
			</div>
			<!--
			<div id="experts-points">
				<ul>
					<li id="experts-bot1" class="active"></li>
					<li id="experts-bot2"></li>
				</ul>
			</div>
		-->

		</div>

	</div>
</div>


