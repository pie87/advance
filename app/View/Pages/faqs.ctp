<div class="content-wrapp blue-degree-bg">
	<div class="wrapp">

		<div class="banner-head">
			<div class="banner-head-left">
				<img src="/img/img-FAQS-v3.png" />
			</div>
			<div class="banner-faqs-right-small">
				<p>
					“I could not believe I could just<br/>
					call and talk to the doctor who<br/>
					was leading me through my case.”<br/>
					<div class="banner-head-name blue">
						<b>THOMAS,</b> SEATTLE 
					</div>
				</p>
			</div>
		</div>

	</div>
</div>

<div class="content-wrapp header-bg">
	<div class="wrapp">
		<div class="submenu-wrapp"></div>
	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">

		<div class="faqs-infobox">

			<div class="faqs-info-title dark-blue">
				GENERAL INFORMATION
			</div>

			<div class="faqs-subtitle blue-sea">
				Why do I need an Expert Medical Opinion?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>Using the Expert Medical Opinion program makes sense if you need more information in order to feel comfortable with a serious health-related decision.</p>
				<p>The quality of your medical care relies on the good judgment of medical professionals. No physician can keep current with all recent medical findings and two minds thinking about your health are better than one. Getting an Expert Medical Opinion ensures that you have additional information from world class experts so that you can make the right decision about your next steps.</p>
			</div>

			<div class="faqs-subtitle blue-sea">
				At what point should I ask for an Expert Medical Opinion?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>The service can be used at any point during the care process.</p>
			</div>

			<div class="faqs-subtitle blue-sea">
				Which diseases or conditions can be reviewed by experts?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>As our selection of experts focuses on the specific disease and not on the medical specialty, we can address all types of diagnoses or questions. We work with the full range of medical issues.</p>
			</div>

			<div class="faqs-subtitle blue-sea">
				Can I use the service for a preexisting condition?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>Absolutely. The service can be used by people with preexisting conditions or new conditions.</p>
			</div>

			<div class="faqs-subtitle blue-sea">
				What do I get with an Expert Medical Opinion?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
			<p>- Unrestricted access to a dedicated doctor by telephone.</p>

				<p>- Reports from leading global experts who have reviewed your medical information.</p>

				<p>- Concierge services such as medical record collection, provider vetting, and (when possible) expedited appointments with face-to-face providers.</p>

				<p>- Information on resources available to you through your insurance carrier or employer.</p>
			</div>

			<div class="faqs-subtitle blue-sea">
				How does an Expert Medical Opinion differ from a face-to-face second opinion or an internet second opinion?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>As part of the Expert Medical Opinion, you work with a dedicated doctor who has the time and expertise to be both your advocate and advisor. Having this extra physician-level support empowers you to get the most out of the important analysis contained in the experts’ reports.</p>
			</div>

			<div class="faqs-subtitle blue-sea">
				How do we decide which experts to use for each case?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>We know that this is the most important decision we make for each patient. Although we carefully consider several factors while choosing experts for each case, our highest priority is the selection of world class experts with successful track records treating patients with the same condition.</p>
			</div>

			<div class="faqs-subtitle blue-sea">
				What questions do people usually ask the experts?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>Experts will always comment on your diagnosis and treatment plan, but you will also be given the opportunity to ask them a list of questions to help you clarify your medical situation. Patients’ questions are normally very specific to their particular medical situation - your Physician Case Manager will work with you to develop this list.</p> 
				<p><span class="blue-sea"><b>Here are examples of questions patients ask:</b></span></p>
				<p>- Is this procedure appropriate at this time?</p>
				<p>- Are there additional risks if I wait a few months before doing this procedure?</p>
				<p>- Is my condition inheritable? Are there genetic tests to see if my children are at risk?</p>
				<p>- Are the outcomes of this procedure well documented?</p>
				<p>- What changes should I make now in my day to day life?</p>
			</div>

		</div>

		<div class="faqs-infobox">

			<div class="faqs-info-title dark-blue">
				LOGISTICS &amp; PROCESS
			</div>

			<div class="faqs-subtitle blue-sea">
				Do I have to go anywhere in person in order to use this service?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>All services are provided remotely, so there is no need to travel or make an appointment.</p>
			</div>

			<div class="faqs-subtitle blue-sea">
				What if I want to help someone else (such as my mother or daughter) get an Expert Medical Opinion? <img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>Advance Medical works with whoever is coordinating the care for a loved one.</p>
			</div>

			<div class="faqs-subtitle blue-sea">
				What languages do the Physician Case Managers speak?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>To overcome language differences, Advance Medical staffs multilingual Physician Case Managers who together speak more than 20 languages.</p>
			</div>

			<div class="faqs-subtitle blue-sea">
				What happens if the expert is not from my country or does not speak my language?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>We strive to provide the best Expert Medical Opinion for your particular situation and always do a global search for the most relevant experts. We use the services of professional medical translators to overcome language differences between you and the expert.</p>
			</div>

			<div class="faqs-subtitle blue-sea">
				How do I get and transport my medical files?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>Advance Medical will collect your medical records on your behalf. You can also send us your medical files directly.</p>
			</div>

			
		</div>

		<div class="faqs-infobox">

			<div class="faqs-info-title dark-blue">
				CONFIDENTIALITY
			</div>

			<div class="faqs-subtitle blue-sea">
				How is my personal data protected?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>Advance Medical’s data protection systems utilize the most advanced technology available.  Administrative, physical and technical safeguards exceed regulations regarding personal information protection.</p>
			</div>

			<div class="faqs-subtitle blue-sea">
				Will my current doctor (treating physician) find out about this process?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>The choice to involve and inform your treating physician about this process is completely up to you.	</p>
			</div>

			
		</div>

			<div class="faqs-infobox-bottom">

			<div class="faqs-info-title dark-blue">
				PRICING &amp; COVERAGE
			</div>

			<div class="faqs-subtitle blue-sea">
				Is it possible my insurance company or employer will pay for my Expert Medical Opinion?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>Yes. 25 million people globally have existing coverage for this service through their employers or insurance companies. Contact your insurer/employer directly to see if you are already covered.</p>
			</div>

			<div class="faqs-subtitle blue-sea">
				If I am not covered by my employer or insurance plan, how much does it cost to get an Expert Medical Opinion?<img class="slidedown" src="/img/arrow-slidedown2.jpg" />
			</div>

			<div class="faqs-txt closed" rel="0">
				<p>Please call us for information about pricing [<a href="/contact-us">Contact Us</a>].</p>
			</div>

			
		</div>
		

	</div>
</div>
