
<div class="content-wrapp blue-degree-bg">
	<div class="wrapp">

		<div id="slider-wrapp">
			<div id="slider">
				<img id="img1" class="image" src="/img/slides/img-home-slider1.png" />
				<img id="img2" class="image hidden" src="/img/slides/img-slider2.png" />
				<img id="img3" class="image hidden" src="/img/slides/img-home-slider3.png" />
				<img id="img4" class="image hidden" src="/img/slides/img-home-slider4.png" />
				<div id="slider-txt-wrapp">
					<div id="slider-txt-box">
						<div id="slider-txt1" class="slider-txt">
							Advance Medical has been<br/>
							improving patients lives through<br/>
							Expert Medical Opinions<br/>
							since 1999.
						</div>

						<div id="slider-txt2" class="slider-txt hideimg">
							“When you have to make a decision about your personal health, there is nothing more important than feeling confident about the medical information you have been given.”
							<span class="banner-head-name blue">
								<strong>DR. FRANCO M. MUGGIA,</strong> NEW YORK CITY
							</span>
						</div>

						<div id="slider-txt3" class="slider-txt hideimg">
							“As a result of my<br/>
							Expert Medical Opinion,<br/>
							I avoided another painful back surgery<br/>
							and today I’m fully recovered.”
							<span class="banner-head-name blue">
								<strong>Rachel</strong>, Boston 
							</span>
						</div>

						<div id="slider-txt4" class="slider-txt hideimg">
							“The level of qualified doctors<br/>
							this program uses is exceptional.<br/>
							It is an excellent benefit<br/>
							to have available.”<br/>
							<span class="banner-head-name blue">
								<strong>Jordan</strong>, San Francisco
							</span>
						</div>
					</div>
					<div id="points">
						<ul>
							<li id="bot1" class="active" rel="1"></li>
							<li id="bot2" rel="2"></li>
							<li id="bot3" rel="3"></li>
							<li id="bot4" rel="4"></li>
						</ul>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="content-wrapp header-bg">
	<div class="wrapp">
		<div class="submenu-wrapp"></div>
	</div>
</div>

<div class="content-wrapp">
	<div class="wrapp">

		<div id="home-boxes">

			<div class="home-box">

				<div class="home-box-left">
					<div id="animaciobox1_hype_container" style="position:relative;overflow:hidden;width:495px;height:370px;">
							<!-- Load js file from base.js -->	
					</div>
				</div>
				<div class="home-box-right">
					<div class="home-box-title dark-blue">
						OUR VISION
					</div>
					<div class="home-box-title-separador"></div>
					<div class="home-box-txt dark-grey">
						Our vision is to <span class="dark-blue">equip patients<br/>with the opinions of world class<br/>global specialists</span> so that they can<br/>make truly informed decisions<br/>about their health.
					</div>
					<div class="home-box-btn" onClick="window.location='/expert-medical-opinion'">
						<div class="home-box-btn-txt">
							HOW WE DO IT
						</div>
					</div>
				</div>

			</div>

			<div class="home-box">

				<div class="home-box-left2">

					<div class="home-box-txt dark-grey" style="margin-top: 32px;">
						“<span class="dark-blue">Medical issues that can impact your life are critical</span>  - for you and your family.
						<br/>For your own well-being, you owe it to yourself to ensure that you have the best experts available reviewing your case.”
					</div>
					<div class="home-box-txt-s">
						<span class="home-box-txt-s-title dark-blue"><strong>Theodore I. Steinman, M.D.</strong></span><br/>
						<span class="home-box-txt-s-txt blue">
							Clinical Professor of Medicine, Harvard Medical School<br/>Senior Physician,
							Beth Israel Deaconess Medical Center and Brigham &amp; Women's Hospital
						</span>

					</div>
					
				</div>

				<div class="home-box-right3">
					<img src="/img/img-home-box2.png" />
				</div>

			</div>

			<div class="home-box">

				<div class="home-box-map-left">
					<div id="minimapahome2_hype_container" style="position:relative;overflow:hidden;width:545px;height:370px;">
						<!-- Load js file from base.js -->	
					</div>
				</div>
				<div class="home-box-map-right">
					<!--
					<div class="home-box-title dark-blue">
						OUR GLOBAL OFFICES
					</div>
					<div class="home-box-title-separador"></div>
					-->
					<div class="home-box-txt dark-grey">
						<span class="dark-blue">
						Advance Medical covers over <br/>
						25 million patients globally</span>, <br/>
						providing them with the experience <br/>
						and knowledge of the most renowned 
						physicians in the world.
					</div>
					<div class="home-box-btn" style="margin-left:100px;" onClick="window.location='/contact-us'">
						<div class="home-box-btn-txt">
							CONTACT US TODAY
						</div>
					</div>
				</div>


			</div>

		</div>

	</div>
</div>
