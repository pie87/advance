<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Advance Medical</title>
<link rel="shortcut icon" href="/img/favicon-adbance-medical2.ico" />

<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">
<META NAME="ROBOTS" CONTENT="INDEX, FOLLOW">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!--[if lt IE 9]> 
 <script type="text/javascript">    
 	document.createElement("nav");    
 	document.createElement("header");    
 	document.createElement("footer");    
 	document.createElement("section");    
 	document.createElement("article");    
 	document.createElement("aside");    
 	document.createElement("hgroup"); 
 </script> 
 <![endif]-->

<link rel="stylesheet" type="text/css" href="/css/base.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/css/base-smartphone.css" media="screen" />

<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,300italic' rel='stylesheet' type='text/css'>


<script type="text/javascript">aqui='<?php $hola=$this->here; echo substr($hola,4,strlen($hola)); ?>';</script>
<script type="text/javascript" src="/js/jquery-2.0.3.js"></script>
<script type="text/javascript" src="/js/easing.js"></script>
<script type="text/javascript" src="/js/animatecolors.js"></script>
<script type="text/javascript" src="/js/jquery.visible.min.js"></script>
<script type="text/javascript" src="/js/base.js"></script>
<!--<script type="text/javascript" src="/js/modernizr.custom.11735.js"></script>-->

<!-- PIE GRAPH -->
<script type="text/javascript" src="/js/piechart/jquery.easypiechart.js"></script>
<link rel="stylesheet" type="text/css" href="/js/piechart/style.css">
<!-- END PIE GRAPH -->

<?php
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0;') !== false) {  
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'rv:11.0') !== false) { 
?>
			<link rel="stylesheet" type="text/css" href="/css/all-ie-base.css" />
<?php
		}
	}
?>


<!--[if IE]>
	<link rel="stylesheet" type="text/css" href="/css/all-ie-base.css" />
<![endif]-->

</head>

<body>

<?php
/*echo $_SERVER['HTTP_USER_AGENT'];*/
?>

<div class="center-smart">
	<div class="center-header-smartphone">

		<div class="content-wrapp header-bg" id="top">
			<div class="wrapp">
				<a href="#top"><div class="scrolltop-wrapp"><div class="scrolltop"></div><div class="scrolltop-roll"></div></div></a>
				<header>

					<nav class="submenu-wrapp">

						<ul id="submenu">
							<li><a href="/about-us" <?php if($this->here=="/about-us" || $this->here=="/web/about-us") { echo "class='blue'"; } ?>>ABOUT US</a></li>
							<li><a href="/global-offices" <?php if($this->here=="/global-offices" || $this->here=="/web/global-offices") { echo "class='blue'"; } ?>>GLOBAL OFFICES</a></li>
							<li><a href="/contact-us" <?php if($this->here=="/contact-us" || $this->here=="/web/contact-us") { echo "class='blue'"; } ?>>CONTACT US</a></li>
						</ul>

						<div id="lang-arrow"></div>
						<div id="lang-btn">LANGUAGE</div>
						<div id="employers-btn-wrapp">
							<div id="employers-btn" onClick="window.location='/employers'">EMPLOYERS</div>
						</div>

					</nav>

				</header>

			</div>
		</div>

	</div>

	<div class="content-wrapp">
		<div class="wrapp">

			<nav id="menu">

				<div id="logo" onClick="javascript:window.location='/'"></div>

				<ul>
					<li><a href="/expert-medical-opinion" <?php if($this->here=="/expert-medical-opinion" || $this->here=="/web/expert-medical-opinion") { echo "class='blue'"; } ?>>EXPERT<br/>MEDICAL OPINION</a></li>
					<li><a href="/why-advance" <?php if($this->here=="/why-advance" || $this->here=="/web/why-advance") { echo "class='blue'"; } ?>>WHY ADVANCE<br/>MEDICAL</a></li>
					<li class="menu-padding"><a href="/patient-stories" <?php if($this->here=="/patient-stories" || $this->here=="/web/patient-stories") { echo "class='blue'"; } ?>>PATIENT STORIES</a></li>
					<li class="menu-padding"><a href="/experts" <?php if($this->here=="/experts" || $this->here=="/web/experts") { echo "class='blue'"; } ?>>EXPERTS</a></li>
					<li style="margin-right:0px;"><a href="/physician-case-managers" <?php if($this->here=="/physician-case-managers" || $this->here=="/web/physician-case-managers") { echo "class='blue'"; } ?>>PHYSICIAN<br/>CASE MANAGERS</a></li>
				</ul>

			</nav>

		</div>
	</div>

	<div class="center-home-smart">
	<?php echo $content_for_layout; ?>
	</div>	

	<div class="center-footer-smartphone">
		<div class="content-wrapp footer-bg">
			<div class="wrapp">

		
				<footer>

					<div id="footer-left">

						<div id="footer-title"></div>

						<nav id="footermenu-left">
							<ul>
								<li><a href="/expert-medical-opinion">EXPERT MEDICAL OPINION</a></li>
								<li><a href="/why-advance">WHY ADVANCE MEDICAL</a></li>
								<li><a href="/patient-stories">PATIENT STORIES</a></li>
								<li><a href="/experts">EXPERTS</a></li>
								<li><a href="/physician-case-managers">PHYSICIAN CASE MANAGERS</a></li>
								<li><a href="/faqs">FAQS</a></li>
								<li><a class="turq" href="/employers">EMPLOYERS</a></li>
							</ul>
						</nav>

					</div>

					<nav id="footermenu-right">
						<ul>
							<li><a href="/about-us">ABOUT US</a></li>
							<li><a href="/careers">CAREERS</a></li>
							<li><a href="/global-offices">GLOBAL OFFICES</a></li>
							<li><a href="/contact-us">CONTACT US</a></li>
						</ul>
					</nav>

					<div class="footer-sep"></div>

					<nav id="footer-submenu">
						<ul>
							<li><a href="/terms-and-conditions">TERMS AND CONDITIONS</a></li>
							<li><a href="/privacy-notice">PRIVACY NOTICE</a></li>
						</ul>
					</nav>

					<div id="logo-footer"></div>

				</footer>		

			</div>
		</div>
	</div>
</div>
	
</body>
</html>	