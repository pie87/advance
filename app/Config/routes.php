<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
	Router::connect('/expert-medical-opinion', array('controller' => 'pages', 'action' => 'expert_medical'));
	Router::connect('/about-us', array('controller' => 'pages', 'action' => 'about'));
	Router::connect('/why-advance', array('controller' => 'pages', 'action' => 'why_advance'));
	Router::connect('/patient-stories', array('controller' => 'pages', 'action' => 'patient_cases'));
	Router::connect('/experts', array('controller' => 'pages', 'action' => 'experts'));
	Router::connect('/careers', array('controller' => 'pages', 'action' => 'careers'));
	Router::connect('/physician-case-managers', array('controller' => 'pages', 'action' => 'physician'));
	Router::connect('/global-offices', array('controller' => 'pages', 'action' => 'global_offices'));
	Router::connect('/contact-us', array('controller' => 'pages', 'action' => 'contact'));
	Router::connect('/terms-and-conditions', array('controller' => 'pages', 'action' => 'terms'));
	Router::connect('/privacy-notice', array('controller' => 'pages', 'action' => 'privacy'));
	Router::connect('/employers', array('controller' => 'pages', 'action' => 'employers'));
	Router::connect('/faqs', array('controller' => 'pages', 'action' => 'faqs'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

	//Router::connect('/*', array('controller' => 'pages', 'action' => 'error404'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
